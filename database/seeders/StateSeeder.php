<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::insert([
            ['name' => 'حلب'],
            ['name' => 'اللاذقية'],
            ['name' => 'حماة'],
            ['name' => 'إدلب'],
            ['name' => 'حمص'],
            ['name' => 'درعا'],
            ['name' => 'دمشق'],
            ['name' => 'ريف دمشق'],
            ['name' => 'طرطوس'],
            ['name' => 'الرقة'],
            ['name' => 'الحسكة'],
        ]);
    }
}
