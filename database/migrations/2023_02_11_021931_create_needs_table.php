<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('needs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('need_category_id');
            $table->unsignedBigInteger('need_name_id');
            $table->unsignedBigInteger('added_by');
            $table->unsignedBigInteger('donation_center_id');
            $table->text('notes')->nullable();
            $table->double('cost')->default(0);
            $table->integer('quantity')->default(1);
            $table->integer('type')->default(1); // مطلوب - في المستودع - تم توزيعه
            $table->timestamps();

            $table->foreign('added_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('need_category_id')->references('id')->on('need_categories')->onDelete('cascade');
            $table->foreign('need_name_id')->references('id')->on('need_names')->onDelete('cascade');
            $table->foreign('donation_center_id')->references('id')->on('donation_centers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('needs');
    }
};
