<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('age');
            $table->string('image');
            $table->enum('health_condition', [1, 2]); // سليم جريح
            $table->unsignedBigInteger('state_id');
            $table->string('address');
            $table->text('notes')->nullable();
            $table->string('responsible');
            $table->enum('status', [1, 2]); // مفقود تم العثور على ذويه
            $table->unsignedBigInteger('added_by');
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
            $table->foreign('added_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missings');
    }
};
