<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->text('mobile');
            $table->text('notes')->nullable();
            $table->integer('status')->default(1);
            $table->integer('category')->default(1); // غذائية - صحية - مأوى - شخص عالق
            $table->unsignedBigInteger('state_id');
            $table->string('area');
            $table->string('address');
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
};
