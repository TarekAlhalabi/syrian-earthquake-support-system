const random = (min, max) => // eslint-disable-next-line no-mixed-operators
Math.floor(Math.random() * (max - min + 1) + min); // eslint-disable-next-line no-unused-vars

async function confirmDelete(event){
    event.preventDefault();
    var result = false;
    var confirmBox = Swal.fire({
        title: 'هل أنت متأكد؟',
        icon: 'error',
        showCancelButton: true,
        cancelButtonText: 'إلغاء',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'حذف'
      });

      await confirmBox.then((result) => {
        if(result.isConfirmed){
            event.target.parentNode.submit();
        }
      });
}

//# sourceMappingURL=main.js.map