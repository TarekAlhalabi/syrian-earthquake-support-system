@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات الفريق التطوعي #{{ $team->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $team->id }}</b></td>
                        </tr>
                        <tr>
                            <td>المحافظة: </td>
                            <td><b>{{ $team->state->name }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $team->name }}</b></td>
                        </tr>
                        <tr>
                            <td>رقم الهاتف الأساسي: </td>
                            <td><b>{{ $team->primary_mobile }}</b></td>
                        </tr>
                        <tr>
                            <td>رقم الهاتف الثانوي: </td>
                            <td><b>{{ $team->secondary_mobile }}</b></td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $team->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
