@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header"><strong>إضافة فريق</strong></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('teams.store') }}">
                        @csrf
                        <div class="mb-3 row">
                            <div class="col-md-12">
                                <label class="form-label" for="state">المحافظة <b class="text-danger">*</b></label>
                                <select class="form-control" id="state" name="state_id">
                                    <option value="0">--</option>
                                    @foreach ($states as $s)
                                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                                    @endforeach
                                </select>
                                @error('state_id')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="name">الاسم <b class="text-danger">*</b></label>
                                <input class="form-control" name="name" id="name" type="text" value="{{ old('name') }}">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="primary_mobile">رقم الهاتف الأساسي <b class="text-danger">*</b></label>
                                <input class="form-control" name="primary_mobile" id="primary_mobile" type="text" value="{{ old('primary_mobile') }}">
                                @error('primary_mobile')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="secondary_mobile">رقم الهاتف الثانوي <b class="text-danger">*</b></label>
                                <input class="form-control" name="secondary_mobile" id="secondary_mobile" type="text" value="{{ old('secondary_mobile') }}">
                                @error('secondary_mobile')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="col-md-12">
                                <label class="form-label" for="notes">ملاحظات</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="5">{{ old('notes') }}</textarea>
                                @error('notes')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-success" type="submit">إضافة</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
