<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>نظام دعم ضحايا الزلزال</title>
    <link rel="manifest" href="{{ asset('assets/favicon/manifest.json') }}">
    <meta name="theme-color" content="#ffffff">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/avatars/syria.jpg') }}">
    <!-- Vendors styles-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Markazi+Text:wght@400;500;600;700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendors/simplebar/css/simplebar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendors/simplebar.css') }}">
    <!-- Main styles for this application-->
    <link href="{{ asset('css/style-rtl.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/@coreui/chartjs/css/coreui-chartjs.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.css" />
</head>

<body>
    <div class="sidebar sidebar-dark sidebar-fixed" id="sidebar">
        <div class="sidebar-brand d-none d-md-flex">
            <h4>نظام الدعم</h4>
        </div>
        <ul class="sidebar-nav" data-coreui="navigation" data-simplebar="">
            <li class="nav-item"><a class="nav-link" href="{{ route('dashboard') }}">
                    <svg class="nav-icon">
                        <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-speedometer') }}"></use>
                    </svg> لوحة التحكم</a></li>
            <li class="nav-title">إدارة النظام</li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-user') }}"></use>
                </svg> المفقودين</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('missings.index') }}"><span
                                class="nav-icon"></span> بيانات المفقودين</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('missings.create') }}"><span
                                class="nav-icon"></span> إضافة حالة</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-building') }}"></use>
                </svg> مراكز الإيواء</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('shelters.index') }}"><span
                                class="nav-icon"></span> مراكز الإيواء</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('shelters.create') }}"><span
                                class="nav-icon"></span> إضافة مركز</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-apple') }}"></use>
                </svg> المساعدات الغذائية</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('foods.index') }}"><span
                                class="nav-icon"></span> إدارة المساعدات</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('foods.create') }}"><span
                                class="nav-icon"></span> إضافة طلب</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-car-alt') }}"></use>
                </svg> طلبات النقل والمواصلات</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('deliveries.index') }}"><span
                                class="nav-icon"></span> إدارة الطلبات</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('deliveries.create') }}"><span
                                class="nav-icon"></span> إضافة طلب</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-heart') }}"></use>
                </svg> التطوع</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('supports.index') }}"><span
                                class="nav-icon"></span> إدارة المتطوعين</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('supports.create') }}"><span
                                class="nav-icon"></span> إضافة طلب متطوع</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-envelope-letter') }}"></use>
                </svg> الطلبات الواردة</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('requests.index') }}"><span
                                class="nav-icon"></span> إدارة الطلبات الواردة</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-info') }}"></use>
                </svg> دليل الفرق التطوعية</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('teams.index') }}"><span
                                class="nav-icon"></span> إدارة الفرق</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('teams.create') }}"><span
                                class="nav-icon"></span> إضافة فريق</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-money') }}"></use>
                </svg> دليل حملات جمع التبرعات</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('donations.index') }}"><span
                                class="nav-icon"></span> إدارة الحملات</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('donations.create') }}"><span
                                class="nav-icon"></span> إضافة حملة</a></li>
                </ul>
            </li>
            @if (auth()->user()->role == 'admin')
                <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                    <svg class="nav-icon">
                        <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-bell') }}"></use>
                    </svg> تنبيهات البريد الإلكتروني</a>
                    <ul class="nav-group-items">
                        <li class="nav-item"><a class="nav-link" href="{{ route('notifaibles.index') }}"><span
                                    class="nav-icon"></span> إدارة الجهات الواجب إعلامها</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('notifaibles.create') }}"><span
                                    class="nav-icon"></span> إضافة جهة</a></li>
                    </ul>
                </li>
            @endif
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-tag') }}"></use>
                </svg> فئات المواد</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('need_categories.index') }}"><span
                                class="nav-icon"></span> إدارة الفئات</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('need_categories.create') }}"><span
                                class="nav-icon"></span> إضافة فئة</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-file') }}"></use>
                </svg> قائمة أسماء المواد</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('need_names.index') }}"><span
                                class="nav-icon"></span> إدارة أسماء المواد</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('need_names.create') }}"><span
                                class="nav-icon"></span> إضافة اسم مادة</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-home') }}"></use>
                </svg> مراكز جمع التبرعات</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('donation_centers.index') }}"><span
                                class="nav-icon"></span> إدارة مراكز التبرع</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('donation_centers.create') }}"><span
                                class="nav-icon"></span> إضافة مركز تبرع</a></li>
                </ul>
            </li>
            <li class="nav-group"><a class="nav-link nav-group-toggle" href="#">
                <svg class="nav-icon">
                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-basket') }}"></use>
                </svg> إدارة المطلوبات والمخزون</a>
                <ul class="nav-group-items">
                    <li class="nav-item"><a class="nav-link" href="{{ route('needs.index') }}"><span
                                class="nav-icon"></span> إدارة المطلوبات والمخزون</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('needs.create') }}"><span
                                class="nav-icon"></span> إضافة مادة</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="wrapper d-flex flex-column min-vh-100 bg-light">
        <header class="header header-sticky mb-4">
            <div class="container-fluid">
                <button class="header-toggler px-md-0 me-md-3" type="button"
                    onclick="coreui.Sidebar.getInstance(document.querySelector('#sidebar')).toggle()">
                    <svg class="icon icon-lg">
                        <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-menu') }}"></use>
                    </svg>
                </button><a class="header-brand d-md-none" href="#">
                    نظام الدعم</a>
                <ul class="header-nav ms-3">
                    <li class="nav-item dropdown"><a class="nav-link py-0" data-coreui-toggle="dropdown"
                            href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <div class="avatar avatar-md"><img class="avatar-img"
                                    src="{{ asset('assets/img/avatars/syria.jpg') }}" alt="{{ auth()->user()->username }}"></div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end pt-0">
                            <div class="dropdown-header bg-light py-2">
                                <div class="fw-semibold">الحساب</div>
                            </div>
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button type="submit" class="dropdown-item">
                                  <svg class="icon me-2">
                                    <use xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-account-logout') }}"></use>
                                  </svg> تسجيل الخروج</button>
                            </form>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">أهلاً, {{ auth()->user()->username }}</a>
                    </li>
                </ul>
            </div>
        </header>
        <div class="body flex-grow-1 px-3">
            <div class="container-fluid">
                @if (session()->has('success'))
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        </div>
                    </div>
                @endif
                @yield('content')
            </div>
        </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('vendors/@coreui/coreui/js/coreui.bundle.min.js') }}"></script>
    <script src="{{ asset('vendors/simplebar/js/simplebar.min.js') }}"></script>
    <script src="{{ asset('vendors/chart.js/js/chart.min.js') }}"></script>
    <script src="{{ asset('vendors/@coreui/chartjs/js/coreui-chartjs.js') }}"></script>
    <!-- Plugins and scripts required by this view-->
    <script src="{{ asset('vendors/@coreui/utils/js/coreui-utils.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    @yield('scripts')
</body>

</html>
