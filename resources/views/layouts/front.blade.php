<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>دليل المبادرات - زلزال سورية</title>

    <link rel="stylesheet" href="{{ asset('front/plugins/themefisher-font/style.css') }}">
    <link rel="stylesheet" href="{{ asset('front/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/plugins/lightbox2/dist/css/lightbox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/plugins/animate/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('front/plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;500;600;700;800&family=Reem+Kufi+Ink&display=swap" rel="stylesheet">
</head>

<body id="body">
    <div id="preloader">
        <div class='preloader'>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    @yield('content')

    <script src="{{ asset('front/plugins/jquery/jquery.min.js') }}"></script>

    <script src="{{ asset('front/plugins/form-validation/jquery.form.js') }}"></script>
    <script src="{{ asset('front/plugins/form-validation/jquery.validate.min.js') }}"></script>

    <script src="{{ asset('front/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('front/plugins/parallax/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('front/plugins/lightbox2/dist/js/lightbox.min.js') }}"></script>
    <script src="{{ asset('front/plugins/slick/slick.min.js') }}"></script>
    <script src="{{ asset('front/plugins/filterizr/jquery.filterizr.min.js') }}"></script>
    <script src="{{ asset('front/plugins/smooth-scroll/smooth-scroll.min.js') }}"></script>

    <script src="{{ asset('front/js/script.js') }}"></script>

</body>
</html>