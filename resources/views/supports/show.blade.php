@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات طلب متطوعين #{{ $support->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $support->id }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $support->name }}</b></td>
                        </tr>
                        <tr>
                            <td>المسؤول عن المتابعة: </td>
                            <td><b>{{ $support->responsible }}</b></td>
                        </tr>
                        <tr>
                            <td>الحالة: </td>
                            <td>
                                @if ($support->status == 1)
                                    <span class="badge bg-danger text-light">قيد البحث</span>
                                @else
                                    <span class="badge bg-success text-light">مكتمل</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $support->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
