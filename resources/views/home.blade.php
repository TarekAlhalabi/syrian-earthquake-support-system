@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card mb-4 text-white bg-primary">
                <div class="card-body pb-0 d-flex justify-content-between align-items-start">
                    <div>
                        <div class="fs-4 fw-semibold">{{ $missings_count }}</div>
                        <div>عدد المفقودين</div>
                    </div>
                </div>
                <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart1" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card mb-4 text-white bg-info">
                <div class="card-body pb-0 d-flex justify-content-between align-items-start">
                    <div>
                        <div class="fs-4 fw-semibold">{{ $shelters_count }}</div>
                        <div>عدد الملاجئ المتوفرة</div>
                    </div>
                </div>
                <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart2" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card mb-4 text-white bg-warning">
                <div class="card-body pb-0 d-flex justify-content-between align-items-start">
                    <div>
                        <div class="fs-4 fw-semibold">{{ $deliveries_count }}</div>
                        <div>طلبات التوصيل الغير مكتملة</div>
                    </div>
                </div>
                <div class="c-chart-wrapper mt-3" style="height:70px;">
                    <canvas class="chart" id="card-chart3" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card mb-4 text-white bg-danger">
                <div class="card-body pb-0 d-flex justify-content-between align-items-start">
                    <div>
                        <div class="fs-4 fw-semibold">{{ $supports_count }}</div>
                        <div>المتطوعين المطلوبين</div>
                    </div>
                </div>
                <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart4" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header">حالات المفقودين المضافة حديثا</div>
                <div class="card-body">
                    <a href="/" class="btn btn-sm btn-success">إدارة الحالات</a>
                    <br>
                    <hr>
                    <div class="table-responsive mt-3">
                        <table class="table border mb-0">
                            <thead class="table-light fw-semibold">
                                <tr class="align-middle">
                                    <th class="text-center">
                                        الرقم التسلسلي
                                    </th>
                                    <th>الصورة</th>
                                    <th>الاسم</th>
                                    <th>العمر</th>
                                    <th>الحالة الصحية</th>
                                    <th>المحافظة</th>
                                    <th>العنوان</th>
                                    <th>ملاحظات</th>
                                    <th>الفريق المسؤول</th>
                                    <th>الحالة</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($missings as $m)
                                    <tr class="align-middle">
                                        <td class="text-center">
                                            {{ $m->id }}
                                        </td>
                                        <td class="text-center">
                                            <div class="avatar avatar-md"><img class="avatar-img" src="{{ $m->image }}" alt="{{ $m->name }}"></div>
                                        </td>
                                        <td>
                                            {{ $m->name }}
                                        </td>
                                        <td>
                                            {{ $m->age }} سنة
                                        </td>
                                        <td>
                                            {{ $m->health_condition }}
                                        </td>
                                        <td>
                                            {{ $m->state->name }}
                                        </td>
                                        <td>
                                            {{ $m->address }}
                                        </td>
                                        <td>
                                            {{ $m->notes }}
                                        </td>
                                        <td>
                                            {{ $m->responsible }}
                                        </td>
                                        <td>
                                            @if ($m->status == 1)
                                                <span class="badge bg-danger text-light">مفقود</span>
                                            @else
                                                <span class="badge bg-success text-light">تم العثور على ذويه</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('missings.show', ['missing' => $m->id]) }}"
                                                class="btn btn-sm btn-secondary">
                                                <svg class="icon">
                                                    <use
                                                        xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-list') }}">
                                                    </use>
                                                </svg>
                                            </a>
                                            <a href="{{ route('missings.edit', ['missing' => $m->id]) }}"
                                                class="btn btn-sm btn-primary">
                                                <svg class="icon">
                                                    <use
                                                        xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-pencil') }}">
                                                    </use>
                                                </svg>
                                            </a>
                                            <form class="d-inline"
                                                action="{{ route('missings.destroy', ['missing' => $m->id]) }}"
                                                method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger">
                                                    <svg class="icon">
                                                        <use
                                                            xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-trash') }}">
                                                        </use>
                                                    </svg>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="10" class="text-center">لا يوجد بيانات</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        Chart.defaults.pointHitDetectionRadius = 1;
        Chart.defaults.plugins.tooltip.enabled = false;
        Chart.defaults.plugins.tooltip.mode = 'index';
        Chart.defaults.plugins.tooltip.position = 'nearest';
        Chart.defaults.plugins.tooltip.external = coreui.ChartJS.customTooltips;
        Chart.defaults.defaultFontColor = '#646470';


        const cardChart1 = new Chart(document.getElementById('card-chart1'), {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'transparent',
                    borderColor: 'rgba(255,255,255,.55)',
                    pointBackgroundColor: coreui.Utils.getStyle('--cui-primary'),
                    data: [65, 59, 84, 84, 51, 55, 40]
                }]
            },
            options: {
                plugins: {
                    legend: {
                        display: false
                    }
                },
                maintainAspectRatio: false,
                scales: {
                    x: {
                        grid: {
                            display: false,
                            drawBorder: false
                        },
                        ticks: {
                            display: false
                        }
                    },
                    y: {
                        min: 30,
                        max: 89,
                        display: false,
                        grid: {
                            display: false
                        },
                        ticks: {
                            display: false
                        }
                    }
                },
                elements: {
                    line: {
                        borderWidth: 1,
                        tension: 0.4
                    },
                    point: {
                        radius: 4,
                        hitRadius: 10,
                        hoverRadius: 4
                    }
                }
            }
        }); // eslint-disable-next-line no-unused-vars

        const cardChart2 = new Chart(document.getElementById('card-chart2'), {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'transparent',
                    borderColor: 'rgba(255,255,255,.55)',
                    pointBackgroundColor: coreui.Utils.getStyle('--cui-info'),
                    data: [1, 18, 9, 17, 34, 22, 11]
                }]
            },
            options: {
                plugins: {
                    legend: {
                        display: false
                    }
                },
                maintainAspectRatio: false,
                scales: {
                    x: {
                        grid: {
                            display: false,
                            drawBorder: false
                        },
                        ticks: {
                            display: false
                        }
                    },
                    y: {
                        min: -9,
                        max: 39,
                        display: false,
                        grid: {
                            display: false
                        },
                        ticks: {
                            display: false
                        }
                    }
                },
                elements: {
                    line: {
                        borderWidth: 1
                    },
                    point: {
                        radius: 4,
                        hitRadius: 10,
                        hoverRadius: 4
                    }
                }
            }
        }); // eslint-disable-next-line no-unused-vars

        const cardChart3 = new Chart(document.getElementById('card-chart3'), {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'rgba(255,255,255,.2)',
                    borderColor: 'rgba(255,255,255,.55)',
                    data: [78, 81, 80, 45, 34, 12, 40],
                    fill: true
                }]
            },
            options: {
                plugins: {
                    legend: {
                        display: false
                    }
                },
                maintainAspectRatio: false,
                scales: {
                    x: {
                        display: false
                    },
                    y: {
                        display: false
                    }
                },
                elements: {
                    line: {
                        borderWidth: 2,
                        tension: 0.4
                    },
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4
                    }
                }
            }
        }); // eslint-disable-next-line no-unused-vars

        const cardChart4 = new Chart(document.getElementById('card-chart4'), {
            type: 'bar',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                    'October', 'November', 'December', 'January', 'February', 'March', 'April'
                ],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'rgba(255,255,255,.2)',
                    borderColor: 'rgba(255,255,255,.55)',
                    data: [78, 81, 80, 45, 34, 12, 40, 85, 65, 23, 12, 98, 34, 84, 67, 82],
                    barPercentage: 0.6
                }]
            },
            options: {
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false
                    }
                },
                scales: {
                    x: {
                        grid: {
                            display: false,
                            drawTicks: false
                        },
                        ticks: {
                            display: false
                        }
                    },
                    y: {
                        grid: {
                            display: false,
                            drawBorder: false,
                            drawTicks: false
                        },
                        ticks: {
                            display: false
                        }
                    }
                }
            }
        }); // eslint-disable-next-line no-unused-vars

        const mainChart = new Chart(document.getElementById('main-chart'), {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: coreui.Utils.hexToRgba(coreui.Utils.getStyle('--cui-info'), 10),
                    borderColor: coreui.Utils.getStyle('--cui-info'),
                    pointHoverBackgroundColor: '#fff',
                    borderWidth: 2,
                    data: [random(50, 200), random(50, 200), random(50, 200), random(50, 200), random(50,
                        200), random(50, 200), random(50, 200)],
                    fill: true
                }, {
                    label: 'My Second dataset',
                    borderColor: coreui.Utils.getStyle('--cui-success'),
                    pointHoverBackgroundColor: '#fff',
                    borderWidth: 2,
                    data: [random(50, 200), random(50, 200), random(50, 200), random(50, 200), random(50,
                        200), random(50, 200), random(50, 200)]
                }, {
                    label: 'My Third dataset',
                    borderColor: coreui.Utils.getStyle('--cui-danger'),
                    pointHoverBackgroundColor: '#fff',
                    borderWidth: 1,
                    borderDash: [8, 5],
                    data: [65, 65, 65, 65, 65, 65, 65]
                }]
            },
            options: {
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false
                    }
                },
                scales: {
                    x: {
                        grid: {
                            drawOnChartArea: false
                        }
                    },
                    y: {
                        ticks: {
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            stepSize: Math.ceil(250 / 5),
                            max: 250
                        }
                    }
                },
                elements: {
                    line: {
                        tension: 0.4
                    },
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4,
                        hoverBorderWidth: 3
                    }
                }
            }
        });
    </script>
@endsection
