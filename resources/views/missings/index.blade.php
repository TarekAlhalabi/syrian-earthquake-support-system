@extends('layouts.app')

@section('content')
<!-- /.row-->
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header">إدارة بيانات المفقودين</div>
            <div class="card-body">
                <a href="{{ route('missings.create') }}" class="btn btn-sm btn-success">إضافة حالة</a>
                <br><hr>
                <form method="GET" action="{{ route('missings.index') }}" class="row">
                    <div class="col-lg-4 col-md-12">
                        من:
                        <input type="date" name="from" class="form-control form-control-sm" value="{{ request()->get('from') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        إلى:
                        <input type="date" name="to" class="form-control form-control-sm" value="{{ request()->get('to') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        المحافظة:
                        <select class="form-control form-control-sm" name="state">
                            <option value="0" {{ request()->get('state') == 0 ? 'selected' : ''}}>الكل</option>
                            @foreach ($states as $s)
                                <option value="{{ $s->id }}" {{ request()->get('state') == $s->id ? 'selected' : ''}}>{{ $s->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        الاسم:
                        <input type="text" name="subject" class="form-control form-control-sm" value="{{ request()->get('name') }}">
                    </div>
                    <div class="col-lg-3 col-md-12">
                        ترتيب حسب:
                        <select class="form-control form-control-sm" name="sort">
                            <option value="0">الأقدم</option>
                            <option value="1">الأحدث</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        الحالة:
                        <select class="form-control form-control-sm" name="status">
                            <option value="0" {{ request()->get('status') == 0 ? 'selected' : ''}}>الكل</option>
                            <option value="1" {{ request()->get('status') == 1 ? 'selected' : ''}}>مفقود</option>
                            <option value="2" {{ request()->get('status') == 2 ? 'selected' : ''}}>تم العثور على ذويه</option>
                        </select>
                    </div>
                    <div class="col-lg-1 col-md-12">
                        بحث:
                        <div>
                            <button type="submit" class="btn btn-sm btn-primary" name="submit" value="search">بحث</button>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        تصدير:
                        <div>
                            <button class="btn btn-sm btn-success" type="submit" name="submit" value="export_excel">تصدير إلى Excel</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mt-3">
                    <table class="table border mb-0">
                        <thead class="table-light fw-semibold">
                            <tr class="align-middle">
                                <th class="text-center">
                                    الرقم التسلسلي
                                </th>
                                <th>الصورة</th>
                                <th>الاسم</th>
                                <th>العمر</th>
                                <th>الحالة الصحية</th>
                                <th>المحافظة</th>
                                <th>العنوان</th>
                                <th>ملاحظات</th>
                                <th>الفريق المسؤول</th>
                                <th>الحالة</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($missings as $m)
                                <tr class="align-middle">
                                    <td class="text-center">
                                        {{ $m->id }}
                                    </td>
                                    <td class="text-center">
                                        <div class="avatar avatar-lg"><img class="avatar-img" src="{{ $m->image }}" alt="{{ $m->name }}"></div>
                                    </td>
                                    <td>
                                        {{ $m->name }}
                                    </td>
                                    <td>
                                        {{ $m->age }} سنة
                                    </td>
                                    <td>
                                        {{ $m->health_condition }}
                                    </td>
                                    <td>
                                        {{ $m->state->name }}
                                    </td>
                                    <td>
                                        {{ $m->address }}
                                    </td>
                                    <td>
                                        {{ $m->notes }}
                                    </td>
                                    <td>
                                        {{ $m->responsible }}
                                    </td>
                                    <td>
                                        @if ($m->status == 1)
                                            <span class="badge bg-danger text-light">مفقود</span>
                                        @else
                                            <span class="badge bg-success text-light">تم العثور على ذويه</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('missings.show', ['missing' => $m->id]) }}"
                                            class="btn btn-sm btn-secondary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-list') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <a href="{{ route('missings.edit', ['missing' => $m->id]) }}"
                                            class="btn btn-sm btn-primary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-pencil') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <form class="d-inline"
                                            action="{{ route('missings.destroy', ['missing' => $m->id]) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">
                                                <svg class="icon">
                                                    <use
                                                        xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-trash') }}">
                                                    </use>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center">لا يوجد بيانات</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="mt-3">
                    @if ($missings->count() > 0)
                        {{ $missings->withQueryString()->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
@endsection
