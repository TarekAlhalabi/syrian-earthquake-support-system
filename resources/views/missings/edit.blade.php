@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header"><strong>تعديل حالة مفقود</strong></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('missings.update', ['missing' => $missing->id]) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="mb-3 row">
                            <div class="col-md-12">
                                <label class="form-label" for="state">المحافظة <b class="text-danger">*</b></label>
                                <select class="form-control" id="state" name="state_id">
                                    <option value="0">--</option>
                                    @foreach ($states as $s)
                                        <option value="{{ $s->id }}" {{ ($s->id == $missing->state_id) ? 'selected' : '' }}>{{ $s->name }}</option>
                                    @endforeach
                                </select>
                                @error('state_id')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="name">الاسم <b class="text-danger">*</b></label>
                                <input class="form-control" name="name" id="name" type="text" value="{{ $missing->name }}">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="age">العمر <b class="text-danger">*</b></label>
                                <input class="form-control" name="age" id="age" type="number" value="{{ $missing->age }}">
                                @error('age')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-12">
                                <div class="text-center">
                                    <img src="{{ $m->image }}" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="image">الصورة</label>
                                <input class="form-control" name="image" id="image" type="file">
                                @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="health_condition">الحالة الصحية <b class="text-danger">*</b></label>
                                <select class="form-control" id="health_condition" name="health_condition">
                                    <option value="1" {{ ($missing->health_condition == 1) ? 'selected' : '' }}>سليم</option>
                                    <option value="2" {{ ($missing->health_condition == 2) ? 'selected' : '' }}>جريح</option>
                                </select>
                                @error('health_condition')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="address">العنوان <b class="text-danger">*</b></label>
                                <input class="form-control" name="address" id="address" type="text" value="{{ $missing->address }}">
                                @error('address')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="responsible">المسؤول عن المتابعة <b class="text-danger">*</b></label>
                                <input class="form-control" name="responsible" id="responsible" type="text" value="{{ $missing->responsible }}">
                                @error('responsible')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="status">الحالة <b class="text-danger">*</b></label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1" {{ ($missing->status == 1) ? 'selected' : '' }}>مفقود</option>
                                    <option value="2" {{ ($missing->status == 2) ? 'selected' : '' }}>تم العثور على ذويه</option>
                                </select>
                                @error('status')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="col-md-12">
                                <label class="form-label" for="notes">ملاحظات</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="5">{{ $missing->notes }}</textarea>
                                @error('notes')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-primary" type="submit">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
