@extends('layouts.front')

@section('content')
    <section class="contact-us" id="contact">
        <div class="container">
            <div class="row justify-content-center">

                <!-- section title -->
                <div class="col-12">
                    <div class="title text-center">
                        <h2>تسجيل طلب مساعدة غذائية</h2>
                        <p>تسجيل طلب مساعدة كالطعام والشراب وأغذية الأطفال</p>
                        <div class="border"></div>
                    </div>
                </div>
                <div class="contact-form col-md-8">
                    <form method="post" action="{{ route('requests.store') }}">
                        @csrf
                        @if (session()->has('success'))
                            <div class="success text-center">
                                تم تسجيل طلبك بنجاح
                            </div>
                        @endif
                        <br>
                        <input type="hidden" name="category" value="2">
                        <div class="form-group">
                            <input type="text" placeholder="رقم التواصل" class="form-control text-right" name="mobile">
                        </div>
                        <div class="form-group">
                            <select name="state_id" class="form-control text-right">
                                <option value="0">--</option>
                                @foreach ($states as $s)
                                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="المنطقة" class="form-control text-right" name="area">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="العنوان" class="form-control text-right" name="address">
                        </div>
                        <div class="form-group">
                            <textarea rows="10" placeholder="وصف الطلب" class="form-control text-right" name="name" id="message"></textarea>
                        </div>

                        <div id="cf-submit">
                            <input type="submit" id="contact-submit" class="btn btn-transparent" value="تسجيل الطلب">
                        </div>

                    </form>
                </div>
                <!-- ./End Contact Form -->

            </div> <!-- end row -->
        </div> <!-- end container -->
    </section> <!-- end section -->
@endsection
