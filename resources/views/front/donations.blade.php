@extends('layouts.front')

@section('content')
    <!--
            Start About Section
            ==================================== -->
            <section class="service-2 section" id="more">
                <div class="container">
                    <div class="row">

                        <!-- section title -->
                        <div class="col-12">
                            <div class="title text-center">
                                <h2>دليل التبرع</h2>
                                <p>قائمة بالجهات الرسمية والفرق التطوعية التي تقبل التبرعات لإبصالها للمتضررين</p>
                                <div class="border"></div>
                            </div>
                        </div>
                        <!-- /section title -->
                        <div class="col-md-12">
                            <div class="row text-center">
                                <div class="col-12 mb-4">
                                    <form method="GET" action="/f/donations" class="row justify-content-center">
                                        <div class="col-2 text-right">
                                            <button type="submit" class="btn btn-primary">بحث</button>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" name="name" class="form-control text-right" placeholder="البحث عن اسم" value="{{ request()->get('name') }}">
                                        </div>
                                    </form>
                                </div>

                                @forelse ($donations as $d)
                                    <article class="col-md-4 col-sm-6 col-xs-12 clearfix ">
                                        <div class="post-item">
                                            <div class="media-wrapper">
                                                <img src="{{ $d->image }}" class="img-fluid">
                                            </div>

                                            <div class="content">
                                                <h3>{{ $d->name }}</h3>
                                                <p>{{ $d->notes }}</p>
                                                <a class="btn btn-main" href="#">{{ $d->mobile }}</a>
                                            </div>
                                        </div>
                                    </article>
                                @empty
                                    <div class="col-12 text-center">
                                        <div class="alert alert-danger">لا يوجد بيانات</div>
                                    </div>
                                @endforelse
                            </div>
                            <div class="mt-3">
                                @if ($donations->count() > 0)
                                    {{ $donations->withQueryString()->links() }}
                                @endif
                            </div>
                        </div>
                    </div> <!-- End row -->
                </div> <!-- End container -->
            </section> <!-- End section -->
@endsection