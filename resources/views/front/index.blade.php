@extends('layouts.front')

@section('content')
    <div class="hero-slider">
        <div class="slider-item th-fullpage hero-area" style="background-image: url(front/images/hero.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>نظام دعم متضرري الزلزال في سوريا</h1>
                        <br>
                        <p>تنظيم عمليات البحث عن
                            المفقودين وتقديم المساعدات الغذائية وعمليات التوصيل</p>
                        <br>
                        <a class="btn btn-main"href="/food-support">تسجيل طلب مساعدة غذائية</a>
                        <br>
                        <br>
                        <a class="btn btn-main"href="/stuck-in-rubble-report">التبليغ عن شخص عالق تحت الأنقاض</a>
                        <br>
                        <br>
                        <a class="btn btn-main"href="/shelter-support">تسجيل طلب مأوى</a>
                        <br>
                        <br>
                        <a class="btn btn-main"href="/medical-support">تسجيل طلب مساعدة صحية</a>
                        <br>
                        <br>
                        <br>
                        <br>
                        <a class="btn btn-main bg-primary"href="#more"><i class="tf-ion-ios-arrow-down"></i> المزيد من الخدمات</a>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--
            Start About Section
            ==================================== -->
    <section class="service-2 section" id="more">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <!-- section title -->
                    <div class="title text-center">
                        <h2>دليل الجمعيات والفرق التطوعية</h2>
                        <p>دليل بأسماء ووسائل التواصل مع الفرق والجمعيات التطوعية لمساعدة المتضررين</p>
                        <div class="border"></div>
                    </div>
                    <!-- /section title -->
                </div>
                <div class="col-md-12">
                    <div class="row text-center">
                        @foreach ($teams as $t)
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="service-item">
                                    <h4>{{ $t->name }}</h4>
                                    <p>{{ $t->state->name }} | {{ $t->primary_mobile }} | {{ $t->secondary_mobile }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center mt-4">
                        <a class="btn btn-main"href="/f/teams">تصفح المزيد</a>
                    </div>
                </div>
            </div> <!-- End row -->
        </div> <!-- End container -->
    </section> <!-- End section -->

    <section class="about-2 section" id="about">
        <div class="container">
            <div class="row">

                <!-- section title -->
                <div class="col-12">
                    <div class="title text-center">
                        <h2>دليل التبرع</h2>
                        <p>قائمة بالجهات الرسمية والفرق التطوعية التي تقبل التبرعات لإيصالها للمتضررين</p>
                        <div class="border"></div>
                    </div>
                </div>
                <!-- /section title -->
                <div class="col-md-12">
                    <div class="row text-center">
                        @foreach ($donations as $d)
                            <article class="col-md-4 col-sm-6 col-xs-12 clearfix ">
                                <div class="post-item">
                                    <div class="media-wrapper">
                                        <img src="{{ $d->image }}" class="img-fluid">
                                    </div>

                                    <div class="content">
                                        <h3>{{ $d->name }}</h3>
                                        <p>{{ $d->notes }}</p>
                                        <a class="btn btn-main" href="#">{{ $d->mobile }}</a>
                                    </div>
                                </div>
                            </article>
                        @endforeach
                        <!-- /single blog post -->
                    </div>
                </div>
            </div> <!-- End row -->
            <div class="text-center mt-4">
                <a class="btn btn-main"href="/f/donations">تصفح المزيد</a>
            </div>
        </div> <!-- End container -->
    </section>

    <section class="service-2 section">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <!-- section title -->
                    <div class="title text-center">
                        <h2>دليل مراكز الإيواء</h2>
                        <p>قائمة بمراكز الإيواء وعناوينها</p>
                        <div class="border"></div>
                    </div>
                    <!-- /section title -->
                </div>
                <div class="col-md-12">
                    <div class="row text-center  justify-content-end">
                        @foreach ($shelters as $s)
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="service-item text-right">
                                    <h4>{{ $s->name }}</h4>
                                    <p>العنوان: {{ $s->address }} | المحافظة: {{ $s->state->name }}</p>
                                    <p>المسؤول: {{ $s->responsible }} | السعة: {{ $s->capacity }} شخص</p>
                                    <p>{!! $s->status == 1 ? '<span class="badge badge-success">متاح</span>' : '<span class="badge badge-danger">ممتلئ</span>' !!} :الحالة</p>
                                    <p>معلومات إضافية: 
                                        <br>
                                        {{ $s->notes }}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center mt-4">
                        <a class="btn btn-main"href="/f/shelters">تصفح المزيد</a>
                    </div>
                </div>
            </div> <!-- End row -->
        </div> <!-- End container -->
    </section> <!-- End section -->

    <section class="about-2 section" id="more">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <!-- section title -->
                    <div class="title text-center">
                        <h2>دليل المبادرات الفردية والجماعية</h2>
                        <p>تم جمع معلومات الدليل من خلال مواقع التواصل الاجتماعي وبالتالي نحن نخلي مسؤوليتنا عن أي أرقام وهمية أو جهات غير مرخصة</p>
                        <div class="border"></div>
                    </div>
                    <!-- /section title -->
                </div>
                <div class="col-md-12">
                    <div class="text-center mt-4">
                        <a class="btn btn-main"href="https://docs.google.com/spreadsheets/d/1SrbQq6pr_-uTQ50GoyF4Qfsa5Nqao3XIJuX7tUd2FTo">رابط الدليل</a>
                    </div>
                </div>
            </div> <!-- End row -->
        </div> <!-- End container -->
    </section> <!-- End section -->
@endsection
