@extends('layouts.front')

@section('content')
    <!--
            Start About Section
            ==================================== -->
            <section class="service-2 section" id="more">
                <div class="container">
                    <div class="row">
        
                        <div class="col-12">
                            <!-- section title -->
                            <div class="title text-center">
                                <h2>دليل الجمعيات والفرق التطوعية</h2>
                                <p>دليل بأسماء ووسائل التواصل مع الفرق والجمعيات التطوعية لمساعدة المتضررين</p>
                                <div class="border"></div>
                            </div>
                            <!-- /section title -->
                        </div>
                        <div class="col-md-12">
                            <div class="row text-center">
                                <div class="col-12 mb-4">
                                    <form method="GET" action="/f/teams" class="row justify-content-center">
                                        <div class="col-2 text-right">
                                            <button type="submit" class="btn btn-primary">بحث</button>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" name="name" class="form-control text-right" placeholder="البحث عن اسم" value="{{ request()->get('name') }}">
                                        </div>
                                    </form>
                                </div>
                                @forelse ($teams as $t)
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <div class="service-item">
                                            <h4>{{ $t->name }}</h4>
                                            <p>{{ $t->state->name }} | {{ $t->primary_mobile }} | {{ $t->secondary_mobile }}</p>
                                        </div>
                                    </div>
                                @empty
                                    <div class="col-12 text-center">
                                        <div class="alert alert-danger">لا يوجد بيانات</div>
                                    </div>
                                @endforelse
                            </div>
                            <div class="mt-3">
                                @if ($teams->count() > 0)
                                    {{ $teams->withQueryString()->links() }}
                                @endif
                            </div>
                        </div>
                    </div> <!-- End row -->
                </div> <!-- End container -->
            </section> <!-- End section -->
@endsection