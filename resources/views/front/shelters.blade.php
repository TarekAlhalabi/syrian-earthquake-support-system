@extends('layouts.front')

@section('content')
    <section class="service-2 section">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <!-- section title -->
                    <div class="title text-center">
                        <h2>دليل مراكز الإيواء</h2>
                        <p>قائمة بمراكز الإيواء وعناوينها</p>
                        <div class="border"></div>
                    </div>
                    <!-- /section title -->
                </div>
                <div class="col-md-12">
                    <div class="row text-center justify-content-end">
                        <div class="col-12 mb-4">
                            <form method="GET" action="/f/shelters" class="row justify-content-end">
                                <div class="col-2 text-right">
                                    <button type="submit" class="btn btn-primary">بحث</button>
                                </div>
                                <div class="col-3">
                                    <input type="text" name="name" class="form-control text-right" placeholder="البحث عن اسم" value="{{ request()->get('name') }}">
                                </div>
                                <div class="col-3">
                                    <select name="state" class="form-control text-right">
                                        <option value="0">--</option>
                                        @foreach ($states as $s)
                                                <option value="{{ $s->id }}">{{ $s->name }}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-3">
                                    <select name="status" class="form-control text-right">
                                        <option value="0">--</option>
                                        <option value="1">متاح</option>
                                        <option value="2">ممتلئ</option>
                                    </select>
                                </div>
                            </form>
                        </div>

                        @forelse ($shelters as $s)
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="service-item text-right">
                                    <h4>{{ $s->name }}</h4>
                                    <p>العنوان: {{ $s->address }} | المحافظة: {{ $s->state->name }}</p>
                                    <p>المسؤول: {{ $s->responsible }} | السعة: {{ $s->capacity }} شخص</p>
                                    <p>{!! $s->status == 1
                                        ? '<span class="badge badge-success">متاح</span>'
                                        : '<span class="badge badge-danger">ممتلئ</span>' !!} :الحالة</p>
                                    <p>معلومات إضافية: 
                                        <br>
                                        {{ $s->notes }}
                                    </p>
                                </div>
                            </div>
                        @empty
                            <div class="col-12 text-center">
                                <div class="alert alert-danger">لا يوجد بيانات</div>
                            </div>
                        @endforelse
                    </div>
                    <div class="mt-3">
                        @if ($shelters->count() > 0)
                            {{ $shelters->withQueryString()->links() }}
                        @endif
                    </div>
                </div>
            </div> <!-- End row -->
        </div> <!-- End container -->
    </section> <!-- End section -->
@endsection
