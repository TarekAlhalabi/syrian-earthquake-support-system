@extends('layouts.app')

@section('content')
<!-- /.row-->
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header">إدارة بيانات مراكز التبرع</div>
            <div class="card-body">
                <a href="{{ route('donation_centers.create') }}" class="btn btn-sm btn-success">إضافة مركز تبرع</a>
                <br><hr>
                <form method="GET" action="{{ route('donation_centers.index') }}" class="row">
                    <div class="col-lg-4 col-md-12">
                        من:
                        <input type="date" name="from" class="form-control form-control-sm" value="{{ request()->get('from') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        إلى:
                        <input type="date" name="to" class="form-control form-control-sm" value="{{ request()->get('to') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        المحافظة:
                        <select class="form-control form-control-sm" name="state">
                            <option value="0" {{ request()->get('state') == 0 ? 'selected' : ''}}>الكل</option>
                            @foreach ($states as $s)
                                <option value="{{ $s->id }}" {{ request()->get('state') == $s->id ? 'selected' : ''}}>{{ $s->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        الاسم:
                        <input type="text" name="subject" class="form-control form-control-sm" value="{{ request()->get('name') }}">
                    </div>
                    <div class="col-lg-3 col-md-12">
                        ترتيب حسب:
                        <select class="form-control form-control-sm" name="sort">
                            <option value="0">الأقدم</option>
                            <option value="1">الأحدث</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        بحث:
                        <div>
                            <button type="submit" class="btn btn-sm btn-primary" name="submit" value="search">بحث</button>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        تصدير:
                        <div>
                            <button class="btn btn-sm btn-success" type="submit" name="submit" value="export_excel">تصدير إلى Excel</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mt-3">
                    <table class="table border mb-0">
                        <thead class="table-light fw-semibold">
                            <tr class="align-middle">
                                <th class="text-center">
                                    الرقم التسلسلي
                                </th>
                                <th>الاسم</th>
                                <th>المحافظة</th>
                                <th>المنطقة</th>
                                <th>العنوان</th>
                                <th>المسؤول</th>
                                <th>رقم التواصل</th>
                                <th>ملاحظات</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($donation_centers as $dc)
                                <tr class="align-middle">
                                    <td class="text-center">
                                        {{ $dc->id }}
                                    </td>
                                    <td>
                                        {{ $dc->name }}
                                    </td>
                                    <td>
                                        {{ $dc->state->name }}
                                    </td>
                                    <td>
                                        {{ $dc->area }}
                                    </td>
                                    <td>
                                        {{ $dc->address }}
                                    </td>
                                    <td>
                                        {{ $dc->responsible }}
                                    </td>
                                    <td>
                                        {{ $dc->contact_number }}
                                    </td>
                                    <td>
                                        {{ $dc->notes }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('donation_centers.show', ['donation_center' => $dc->id]) }}"
                                            class="btn btn-sm btn-secondary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-list') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <a href="{{ route('donation_centers.edit', ['donation_center' => $dc->id]) }}"
                                            class="btn btn-sm btn-primary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-pencil') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <form class="d-inline"
                                            action="{{ route('donation_centers.destroy', ['donation_center' => $dc->id]) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">
                                                <svg class="icon">
                                                    <use
                                                        xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-trash') }}">
                                                    </use>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center">لا يوجد بيانات</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="mt-3">
                    @if ($donation_centers->count() > 0)
                        {{ $donation_centers->withQueryString()->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
@endsection
