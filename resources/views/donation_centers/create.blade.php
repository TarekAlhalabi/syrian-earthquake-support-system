@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header"><strong>إضافة مركز تبرع</strong></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('donation_centers.store') }}">
                        @csrf
                        <div class="mb-3 row">
                            <div class="col-md-12">
                                <label class="form-label" for="state">المحافظة <b class="text-danger">*</b></label>
                                <select class="form-control" id="state" name="state_id">
                                    <option value="0">--</option>
                                    @foreach ($states as $s)
                                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                                    @endforeach
                                </select>
                                @error('state_id')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="name">الاسم <b class="text-danger">*</b></label>
                                <input class="form-control" name="name" id="name" type="text" value="{{ old('name') }}">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="area">المنطقة <b class="text-danger">*</b></label>
                                <input class="form-control" name="area" id="area" type="text" value="{{ old('area') }}">
                                @error('area')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="address">العنوان <b class="text-danger">*</b></label>
                                <input class="form-control" name="address" id="address" type="text" value="{{ old('address') }}">
                                @error('address')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="responsible">المسؤول عن المتابعة <b class="text-danger">*</b></label>
                                <input class="form-control" name="responsible" id="responsible" type="text" value="{{ old('responsible') }}">
                                @error('responsible')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="contact_number">رقم التواصل <b class="text-danger">*</b></label>
                                <input class="form-control" name="contact_number" id="contact_number" type="text" value="{{ old('contact_number') }}">
                                @error('contact_number')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="col-md-12">
                                <label class="form-label" for="notes">ملاحظات</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="5">{{ old('notes') }}</textarea>
                                @error('notes')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-success" type="submit">إضافة</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
