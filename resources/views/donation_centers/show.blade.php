@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات مركز التبرع #{{ $donation_center->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $donation_center->id }}</b></td>
                        </tr>
                        <tr>
                            <td>المحافظة: </td>
                            <td><b>{{ $donation_center->state->name }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $donation_center->name }}</b></td>
                        </tr>
                        <tr>
                            <td>المنطقة: </td>
                            <td><b>{{ $donation_center->area }}</b></td>
                        </tr>
                        <tr>
                            <td>العنوان: </td>
                            <td><b>{{ $donation_center->address }}</b></td>
                        </tr>
                        <tr>
                            <td>المسؤول عن المتابعة: </td>
                            <td><b>{{ $donation_center->responsible }}</b></td>
                        </tr>
                        <tr>
                            <td>رقم التواصل: </td>
                            <td><b>{{ $donation_center->contact_number }}</b></td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $donation_center->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
