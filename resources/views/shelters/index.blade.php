@extends('layouts.app')

@section('content')
<!-- /.row-->
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header">إدارة بيانات مراكز الإيواء</div>
            <div class="card-body">
                <a href="{{ route('shelters.create') }}" class="btn btn-sm btn-success">إضافة مركز</a>
                <br><hr>
                <form method="GET" action="{{ route('shelters.index') }}" class="row">
                    <div class="col-lg-4 col-md-12">
                        من:
                        <input type="date" name="from" class="form-control form-control-sm" value="{{ request()->get('from') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        إلى:
                        <input type="date" name="to" class="form-control form-control-sm" value="{{ request()->get('to') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        تصدير:
                        <div>
                            <button class="btn btn-sm btn-success" type="submit" name="submit" value="export_excel">تصدير إلى Excel</button>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        الاسم:
                        <input type="text" name="subject" class="form-control form-control-sm" value="{{ request()->get('name') }}">
                    </div>
                    <div class="col-lg-3 col-md-12">
                        ترتيب حسب:
                        <select class="form-control form-control-sm" name="sort">
                            <option value="0">الأقدم</option>
                            <option value="1">الأحدث</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        الحالة:
                        <select class="form-control form-control-sm" name="status">
                            <option value="0" {{ request()->get('status') == 0 ? 'selected' : ''}}>الكل</option>
                            <option value="1" {{ request()->get('status') == 1 ? 'selected' : ''}}>متاح</option>
                            <option value="2" {{ request()->get('status') == 2 ? 'selected' : ''}}>ممتلئ</option>
                        </select>
                    </div>
                    <div class="col-lg-1 col-md-12">
                        بحث:
                        <div>
                            <button type="submit" class="btn btn-sm btn-primary" name="submit" value="search">بحث</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mt-3">
                    <table class="table border mb-0">
                        <thead class="table-light fw-semibold">
                            <tr class="align-middle">
                                <th class="text-center">
                                    الرقم التسلسلي
                                </th>
                                <th>الاسم</th>
                                <th>السعة</th>
                                <th>المحافظة</th>
                                <th>العنوان</th>
                                <th>ملاحظات</th>
                                <th>الفريق المسؤول</th>
                                <th>الحالة</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($shelters as $s)
                                <tr class="align-middle">
                                    <td class="text-center">
                                        {{ $s->id }}
                                    </td>
                                    <td>
                                        {{ $s->name }}
                                    </td>
                                    <td>
                                        {{ $s->capacity }} شخص
                                    </td>
                                    <td>
                                        {{ $s->state->name }}
                                    </td>
                                    <td>
                                        {{ $s->address }}
                                    </td>
                                    <td>
                                        {{ $s->notes }}
                                    </td>
                                    <td>
                                        {{ $s->responsible }}
                                    </td>
                                    <td>
                                        @if ($s->status == 1)
                                            <span class="badge bg-success text-light">متاح</span>
                                        @else
                                            <span class="badge bg-danger text-light">ممتلئ</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('shelters.show', ['shelter' => $s->id]) }}"
                                            class="btn btn-sm btn-secondary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-list') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <a href="{{ route('shelters.edit', ['shelter' => $s->id]) }}"
                                            class="btn btn-sm btn-primary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-pencil') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <form class="d-inline"
                                            action="{{ route('shelters.destroy', ['shelter' => $s->id]) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">
                                                <svg class="icon">
                                                    <use
                                                        xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-trash') }}">
                                                    </use>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center">لا يوجد بيانات</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="mt-3">
                    @if ($shelters->count() > 0)
                        {{ $shelters->withQueryString()->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
@endsection
