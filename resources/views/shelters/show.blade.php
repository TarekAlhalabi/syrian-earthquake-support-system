@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات مركز الإيواء #{{ $shelter->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $shelter->id }}</b></td>
                        </tr>
                        <tr>
                            <td>المحافظة: </td>
                            <td><b>{{ $shelter->state->name }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $shelter->name }}</b></td>
                        </tr>
                        <tr>
                            <td>السعة: </td>
                            <td><b>{{ $shelter->capacity }}</b></td>
                        </tr>
                        <tr>
                            <td>العنوان: </td>
                            <td><b>{{ $shelter->address }}</b></td>
                        </tr>
                        <tr>
                            <td>المسؤول عن المتابعة: </td>
                            <td><b>{{ $shelter->responsible }}</b></td>
                        </tr>
                        <tr>
                            <td>الحالة: </td>
                            <td>
                                @if ($shelter->status == 1)
                                    <span class="badge bg-success text-light">متاح</span>
                                @else
                                    <span class="badge bg-danger text-light">ممتلئ</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $shelter->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
