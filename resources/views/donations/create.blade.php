@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header"><strong>إضافة حملة تبرع</strong></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('donations.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="name">الاسم <b class="text-danger">*</b></label>
                                <input class="form-control" name="name" id="name" type="text" value="{{ old('name') }}">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="mobile">رقم الهاتف <b class="text-danger">*</b></label>
                                <input class="form-control" name="mobile" id="mobile" type="text" value="{{ old('mobile') }}">
                                @error('mobile')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-12">
                                <label class="form-label" for="image">الصورة</label>
                                <input class="form-control" name="image" id="image" type="file">
                                @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="col-md-12">
                                <label class="form-label" for="notes">ملاحظات <b class="text-danger">*</b></label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="5">{{ old('notes') }}</textarea>
                                @error('notes')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-success" type="submit">إضافة</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
