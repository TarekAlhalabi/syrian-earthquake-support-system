@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات حملة التبرع #{{ $donation->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $donation->id }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $donation->name }}</b></td>
                        </tr>
                        <tr>
                            <td>الصورة: </td>
                            <td><img src="{{ $donation->image }}" alt="" class="img-fluid"></td>
                        </tr>
                        <tr>
                            <td>رقم الهاتف: </td>
                            <td><b>{{ $donation->mobile }}</b></td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $donation->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
