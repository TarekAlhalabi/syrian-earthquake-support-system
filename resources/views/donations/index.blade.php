@extends('layouts.app')

@section('content')
<!-- /.row-->
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header">إدارة بيانات حملات التبرع</div>
            <div class="card-body">
                <a href="{{ route('donations.create') }}" class="btn btn-sm btn-success">إضافة حملة</a>
                <br><hr>
                <form method="GET" action="{{ route('donations.index') }}" class="row">
                    <div class="col-lg-4 col-md-12">
                        من:
                        <input type="date" name="from" class="form-control form-control-sm" value="{{ request()->get('from') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        إلى:
                        <input type="date" name="to" class="form-control form-control-sm" value="{{ request()->get('to') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        تصدير:
                        <div>
                            <button class="btn btn-sm btn-success" type="submit" name="submit" value="export_excel">تصدير إلى Excel</button>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        الاسم:
                        <input type="text" name="subject" class="form-control form-control-sm" value="{{ request()->get('name') }}">
                    </div>
                    <div class="col-lg-3 col-md-12">
                        ترتيب حسب:
                        <select class="form-control form-control-sm" name="sort">
                            <option value="0">الأقدم</option>
                            <option value="1">الأحدث</option>
                        </select>
                    </div>
                    <div class="col-lg-1 col-md-12">
                        بحث:
                        <div>
                            <button type="submit" class="btn btn-sm btn-primary" name="submit" value="search">بحث</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mt-3">
                    <table class="table border mb-0">
                        <thead class="table-light fw-semibold">
                            <tr class="align-middle">
                                <th class="text-center">
                                    الرقم التسلسلي
                                </th>
                                <th>الصورة</th>
                                <th>الاسم</th>
                                <th>رقم الهاتف</th>
                                <th>ملاحظات</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($donations as $d)
                                <tr class="align-middle">
                                    <td class="text-center">
                                        {{ $d->id }}
                                    </td>
                                    <td class="text-center">
                                        <div class="avatar avatar-lg"><img class="avatar-img" src="{{ $d->image }}" alt="{{ $d->name }}"></div>
                                    </td>
                                    <td>
                                        {{ $d->name }}
                                    </td>
                                    <td>
                                        {{ $d->name }}
                                    </td>
                                    <td>
                                        {{ $d->mobile }}
                                    </td>
                                    <td>
                                        {{ $d->notes }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('donations.show', ['donation' => $d->id]) }}"
                                            class="btn btn-sm btn-secondary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-list') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <a href="{{ route('donations.edit', ['donation' => $d->id]) }}"
                                            class="btn btn-sm btn-primary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-pencil') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <form class="d-inline"
                                            action="{{ route('donations.destroy', ['donation' => $d->id]) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">
                                                <svg class="icon">
                                                    <use
                                                        xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-trash') }}">
                                                    </use>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center">لا يوجد بيانات</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="mt-3">
                    @if ($donations->count() > 0)
                        {{ $donations->withQueryString()->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
@endsection
