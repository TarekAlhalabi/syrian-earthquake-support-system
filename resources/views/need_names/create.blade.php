@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-6">
            <div class="card mb-4">
                <div class="card-header"><strong>إضافة اسم مادة</strong></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('need_names.store') }}">
                        @csrf
                        <div class="mb-3 row">
                            <div class="col-md-12">
                                <label class="form-label" for="name">الاسم <b class="text-danger">*</b></label>
                                <input class="form-control" name="name" id="name" type="text" value="{{ old('name') }}">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-success" type="submit">إضافة</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
