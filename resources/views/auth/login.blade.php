@extends('layouts.auth')

@section('content')
<div class="col-lg-8">
    <div class="card-group d-block d-md-flex row text-end">
        <div class="card col-md-5 text-white bg-primary py-5 text-center">
            <div class="card-body">
                <div>
                    <h2>نظام الدعم</h2>
                    <p>نظام إلكتروني لتوحيد جهود الفرق والجمعيات والمنظمات في دعم ضحايا زلزال سوريا 2023</p>
                </div>
            </div>
        </div>
        <div class="card col-md-7 p-4 mb-0">
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="card-body">
                    <h1>تسجيل الدخول</h1>
                    <p class="text-medium-emphasis">ادخل اسم المستخدم وكلمة السر للدخول إلى النظام</p>
                    <div class="input-group mb-3"><span class="input-group-text">
                            <svg class="icon">
                                <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                            </svg></span>
                        <input class="form-control" type="text" placeholder="اسم المستخدم" name="username"
                            value="{{ old('username') }}">
                    </div>
                    @error('username')
                        <div class="alert alert-danger" role="alert">{{ $message }}</div>
                    @enderror
                    <div class="input-group mb-4"><span class="input-group-text">
                            <svg class="icon">
                                <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-lock-locked">
                                </use>
                            </svg></span>
                        <input class="form-control" type="password" placeholder="كلمة المرور"
                            name="password">
                    </div>
                    @error('password')
                        <div class="alert alert-danger" role="alert">{{ $message }}</div>
                    @enderror
                    <div class="row text-end">
                        <div class="col-12">
                            <button class="btn btn-primary px-4" type="submit">دخول</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection