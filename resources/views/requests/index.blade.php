@extends('layouts.app')

@section('content')
<!-- /.row-->
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header">إدارة الطلبات</div>
            <div class="card-body">
                <form method="GET" action="{{ route('requests.index') }}" class="row">
                    <div class="col-lg-4 col-md-12">
                        من:
                        <input type="date" name="from" class="form-control form-control-sm" value="{{ request()->get('from') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        إلى:
                        <input type="date" name="to" class="form-control form-control-sm" value="{{ request()->get('to') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        تصدير:
                        <div>
                            <button class="btn btn-sm btn-success" type="submit" name="submit" value="export_excel">تصدير إلى Excel</button>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        الطلب:
                        <input type="text" name="subject" class="form-control form-control-sm" value="{{ request()->get('name') }}">
                    </div>
                    <div class="col-lg-3 col-md-12">
                        ترتيب حسب:
                        <select class="form-control form-control-sm" name="sort">
                            <option value="0">الأقدم</option>
                            <option value="1">الأحدث</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        الحالة:
                        <select class="form-control form-control-sm" name="status">
                            <option value="0" {{ request()->get('status') == 0 ? 'selected' : ''}}>الكل</option>
                            <option value="1" {{ request()->get('status') == 1 ? 'selected' : ''}}>قيد المتابعة</option>
                            <option value="2" {{ request()->get('status') == 2 ? 'selected' : ''}}>مكتمل</option>
                        </select>
                    </div>
                    <div class="col-lg-1 col-md-12">
                        بحث:
                        <div>
                            <button type="submit" class="btn btn-sm btn-primary" name="submit" value="search">بحث</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mt-3">
                    <table class="table border mb-0">
                        <thead class="table-light fw-semibold">
                            <tr class="align-middle">
                                <th class="text-center">
                                    الرقم التسلسلي
                                </th>
                                <th>الطلب</th>
                                <th>الفئة</th>
                                <th>المحافظة</th>
                                <th>المنطقة</th>
                                <th>العنوان</th>
                                <th>الملاحظات</th>
                                <th>رقم التواصل</th>
                                <th>الحالة</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($requests as $r)
                                <tr class="align-middle">
                                    <td class="text-center">
                                        {{ $r->id }}
                                    </td>
                                    <td>
                                        {{ $r->name }}
                                    </td>
                                    <td>
                                        {{ $r->category }}
                                    </td>
                                    <td>
                                        {{ $r->state->name }}
                                    </td>
                                    <td>
                                        {{ $r->area }}
                                    </td>
                                    <td>
                                        {{ $r->address }}
                                    </td>
                                    <td>
                                        {{ $r->notes }}
                                    </td>
                                    <td>
                                        {{ $r->mobile }}
                                    </td>
                                    <td>
                                        @if ($r->status == 1)
                                            <span class="badge bg-danger text-light">قيد المتابعة</span>
                                        @else
                                            <span class="badge bg-success text-light">مكتمل</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('requests.edit', ['request' => $r->id]) }}"
                                            class="btn btn-sm btn-primary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-pencil') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <form class="d-inline"
                                            action="{{ route('requests.destroy', ['request' => $r->id]) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">
                                                <svg class="icon">
                                                    <use
                                                        xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-trash') }}">
                                                    </use>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center">لا يوجد بيانات</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="mt-3">
                    @if ($requests->count() > 0)
                        {{ $requests->withQueryString()->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
@endsection
