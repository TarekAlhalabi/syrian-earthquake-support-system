@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header"><strong>تعديل الطلب</strong></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('requests.update', ['request' => $request->id]) }}">
                        @csrf
                        @method('PUT')
                        <div class="mb-3 row">
                            <div class="col-md-12">
                                <p>الطلب: 
                                    {{ $request->name }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>رقم الموبايل: 
                                    {{ $request->mobile }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>المحافظة: 
                                    {{ $request->state->name }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>المنطقة: 
                                    {{ $request->area }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>العنوان: 
                                    {{ $request->address }}
                                </p>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="status">الحالة <b class="text-danger">*</b></label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1" {{ ($request->status == 1) ? 'selected' : '' }}>قيد المتابعة</option>
                                    <option value="2" {{ ($request->status == 2) ? 'selected' : '' }}>مكتمل</option>
                                </select>
                                @error('status')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="col-md-12">
                                <label class="form-label" for="notes">ملاحظات</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="5">{{ $request->notes }}</textarea>
                                @error('notes')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-primary" type="submit">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
