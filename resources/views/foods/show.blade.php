@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات طلب العذاء #{{ $food->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $food->id }}</b></td>
                        </tr>
                        <tr>
                            <td>المحافظة: </td>
                            <td><b>{{ $food->state->name }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $food->name }}</b></td>
                        </tr>
                        <tr>
                            <td>الكمية: </td>
                            <td><b>{{ $food->quantity }}</b></td>
                        </tr>
                        <tr>
                            <td>العنوان: </td>
                            <td><b>{{ $food->address }}</b></td>
                        </tr>
                        <tr>
                            <td>المسؤول عن المتابعة: </td>
                            <td><b>{{ $food->responsible }}</b></td>
                        </tr>
                        <tr>
                            <td>الحالة: </td>
                            <td>
                                @if ($food->status == 1)
                                    <span class="badge bg-danger text-light">قيد الطلب</span>
                                @else
                                    <span class="badge bg-success text-light">مكتمل</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $food->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
