@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header"><strong>تعديل بيانات العنصر</strong></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('needs.update', ['need' => $need->id]) }}">
                        @csrf
                        @method('PUT')
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="need_category_id">نوع العنصر <b class="text-danger">*</b></label>
                                <select class="form-control" id="need_category_id" name="need_category_id">
                                    <option value="0">--</option>
                                    @foreach ($need_categories as $nc)
                                        <option value="{{ $nc->id }}" {{ ($nc->id == $need->need_category_id) ? 'selected' : '' }}>{{ $nc->name }}</option>
                                    @endforeach
                                </select>
                                @error('need_category_id')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="donation_center_id">مركز التبرع <b class="text-danger">*</b></label>
                                <select class="form-control" id="donation_center_id" name="donation_center_id">
                                    <option value="0">--</option>
                                    @foreach ($donation_centers as $dc)
                                        <option value="{{ $dc->id }}" {{ ($dc->id == $need->donation_center_id) ? 'selected' : '' }}>{{ $dc->name }}</option>
                                    @endforeach
                                </select>
                                @error('donation_center_id')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="need_name_id">اسم المادة <b class="text-danger">*</b></label>
                                <select class="form-control" id="need_name_id" name="need_name_id">
                                    <option value="0">--</option>
                                    @foreach ($need_names as $nn)
                                        <option value="{{ $nn->id }}" {{ ($nn->id == $need->need_name_id) ? 'selected' : '' }}>{{ $nn->name }}</option>
                                    @endforeach
                                </select>
                                @error('need_name_id')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="quantity">الكمية <b class="text-danger">*</b></label>
                                <input class="form-control" name="quantity" id="quantity" type="number" value="{{ $need->quantity }}">
                                @error('quantity')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="cost">التكلفة المقدرة <b class="text-danger">*</b></label>
                                <input class="form-control" name="cost" id="cost" type="text" value="{{ $need->cost }}">
                                @error('cost')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="type">الصفة <b class="text-danger">*</b></label>
                                <select class="form-control" id="type" name="type">
                                    <option value="1" {{ ($need->type == 1) ? 'selected' : '' }}>مطلوب</option>
                                    <option value="2" {{ ($need->type == 2) ? 'selected' : '' }}>في المستودع</option>
                                    <option value="3" {{ ($need->type == 3) ? 'selected' : '' }}>تم توزيعه</option>
                                </select>
                                @error('type')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="col-md-12">
                                <label class="form-label" for="notes">ملاحظات</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="5">{{ $need->notes }}</textarea>
                                @error('notes')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-primary" type="submit">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
