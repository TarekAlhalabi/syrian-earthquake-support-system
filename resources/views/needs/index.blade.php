@extends('layouts.app')

@section('content')
<!-- /.row-->
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header">إدارة بيانات العناصر</div>
            <div class="card-body">
                <a href="{{ route('needs.create') }}" class="btn btn-sm btn-success">إضافة عنصر</a>
                <br><hr>
                <form method="GET" action="{{ route('needs.index') }}" class="row">
                    <div class="col-lg-4 col-md-12">
                        من:
                        <input type="date" name="from" class="form-control form-control-sm" value="{{ request()->get('from') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        إلى:
                        <input type="date" name="to" class="form-control form-control-sm" value="{{ request()->get('to') }}">
                    </div>
                    <div class="col-lg-4 col-md-12">
                        مركز التبرع:
                        <select class="form-control form-control-sm" name="donation_center">
                            <option value="0" {{ request()->get('donation_center') == 0 ? 'selected' : ''}}>الكل</option>
                            @foreach ($donation_centers as $dc)
                                <option value="{{ $dc->id }}" {{ request()->get('donation_center') == $dc->id ? 'selected' : ''}}>{{ $dc->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        نوع العنصر:
                        <select class="form-control form-control-sm" name="need_category">
                            <option value="0" {{ request()->get('need_category') == 0 ? 'selected' : ''}}>الكل</option>
                            @foreach ($need_categories as $nc)
                                <option value="{{ $nc->id }}" {{ request()->get('need_category') == $nc->id ? 'selected' : ''}}>{{ $nc->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        الاسم:
                        <input type="text" name="subject" class="form-control form-control-sm" value="{{ request()->get('name') }}">
                    </div>
                    <div class="col-lg-3 col-md-12">
                        ترتيب حسب:
                        <select class="form-control form-control-sm" name="sort">
                            <option value="0">الأقدم</option>
                            <option value="1">الأحدث</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        الصفة:
                        <select class="form-control form-control-sm" name="status">
                            <option value="0" {{ request()->get('type') == 0 ? 'selected' : ''}}>الكل</option>
                            <option value="1" {{ request()->get('type') == 1 ? 'selected' : ''}}>مطلوب</option>
                            <option value="2" {{ request()->get('type') == 2 ? 'selected' : ''}}>في المستودع</option>
                            <option value="3" {{ request()->get('type') == 2 ? 'selected' : ''}}>تم توزيعه</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        بحث:
                        <div>
                            <button type="submit" class="btn btn-sm btn-primary" name="submit" value="search">بحث</button>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12">
                        تصدير:
                        <div>
                            <button class="btn btn-sm btn-success" type="submit" name="submit" value="export_excel">تصدير إلى Excel</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mt-3">
                    <table class="table border mb-0">
                        <thead class="table-light fw-semibold">
                            <tr class="align-middle">
                                <th class="text-center">
                                    الرقم التسلسلي
                                </th>
                                <th>الاسم</th>
                                <th>نوع العنصر</th>
                                <th>مركز التبرع</th>
                                <th>الكمية</th>
                                <th>التكلفة المقدرة</th>
                                <th>الصفة</th>
                                <th>الملاحظات</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($needs as $n)
                                <tr class="align-middle">
                                    <td class="text-center">
                                        {{ $n->id }}
                                    </td>
                                    <td>
                                        {{ $n->need_name->name }}
                                    </td>
                                    <td>
                                        {{ $n->need_category->name }}
                                    </td>
                                    <td>
                                        {{ $n->donation_center->name }}
                                    </td>
                                    <td>
                                        {{ $n->quantity }}
                                    </td>
                                    <td>
                                        {{ $n->cost }}
                                    </td>
                                    <td>
                                        @if ($n->type == 1)
                                            <span class="badge bg-danger text-light">مطلوب</span>
                                        @elseif ($n->type == 2)
                                            <span class="badge bg-warning text-light">في المستودع</span>
                                        @else
                                            <span class="badge bg-warning text-light">تم تسليمه</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $n->notes }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('needs.show', ['need' => $n->id]) }}"
                                            class="btn btn-sm btn-secondary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-list') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <a href="{{ route('needs.edit', ['need' => $n->id]) }}"
                                            class="btn btn-sm btn-primary">
                                            <svg class="icon">
                                                <use
                                                    xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-pencil') }}">
                                                </use>
                                            </svg>
                                        </a>
                                        <form class="d-inline"
                                            action="{{ route('needs.destroy', ['need' => $n->id]) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">
                                                <svg class="icon">
                                                    <use
                                                        xlink:href="{{ asset('vendors/@coreui/icons/svg/free.svg#cil-trash') }}">
                                                    </use>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center">لا يوجد بيانات</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="mt-3">
                    @if ($needs->count() > 0)
                        {{ $needs->withQueryString()->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
@endsection
