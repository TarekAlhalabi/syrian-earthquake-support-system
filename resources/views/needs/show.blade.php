@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات العنصر #{{ $need->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $need->id }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $need->need_name->name }}</b></td>
                        </tr>
                        <tr>
                            <td>نوع العنصر: </td>
                            <td><b>{{ $need->need_category->name }}</b></td>
                        </tr>
                        <tr>
                            <td>مركز التبرع: </td>
                            <td><b>{{ $need->donation_center->name }}</b></td>
                        </tr>
                        <tr>
                            <td>الكمية: </td>
                            <td><b>{{ $need->quantity }}</b></td>
                        </tr>
                        <tr>
                            <td>الكلفة المقدرة: </td>
                            <td><b>{{ $need->cost }}</b></td>
                        </tr>
                        <tr>
                            <td>الصفة: </td>
                            <td>
                                @if ($need->status == 1)
                                    <span class="badge bg-danger text-light">مطلوب</span>
                                @elseif ($need->status == 2)
                                    <span class="badge bg-warning text-light">في المستودع</span>
                                @else
                                    <span class="badge bg-warning text-light">تم تسليمه</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $need->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
