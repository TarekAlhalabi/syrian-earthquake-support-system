@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات طلب النقل #{{ $delivery->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $delivery->id }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $delivery->name }}</b></td>
                        </tr>
                        <tr>
                            <td>من: </td>
                            <td><b>{{ $delivery->from }}</b></td>
                        </tr>
                        <tr>
                            <td>إلى: </td>
                            <td><b>{{ $delivery->to }}</b></td>
                        </tr>
                        <tr>
                            <td>المسؤول عن المتابعة: </td>
                            <td><b>{{ $delivery->responsible }}</b></td>
                        </tr>
                        <tr>
                            <td>الحالة: </td>
                            <td>
                                @if ($delivery->status == 1)
                                    <span class="badge bg-danger text-light">قيد التهيئة</span>
                                @else
                                    <span class="badge bg-success text-light">مكتمل</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $delivery->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
