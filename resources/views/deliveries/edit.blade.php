@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header"><strong>تعديل بيانات طلب النقل</strong></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('deliveries.update', ['delivery' => $delivery->id]) }}">
                        @csrf
                        @method('PUT')
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="name">الاسم <b class="text-danger">*</b></label>
                                <input class="form-control" name="name" id="name" type="text" value="{{ $delivery->name }}">
                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="from">من <b class="text-danger">*</b></label>
                                <input class="form-control" name="from" id="from" type="text" value="{{ $delivery->from }}">
                                @error('from')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="to">إلى <b class="text-danger">*</b></label>
                                <input class="form-control" name="to" id="to" type="text" value="{{ $delivery->to }}">
                                @error('to')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="responsible">المسؤول عن المتابعة <b class="text-danger">*</b></label>
                                <input class="form-control" name="responsible" id="responsible" type="text" value="{{ $delivery->responsible }}">
                                @error('responsible')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <label class="form-label" for="status">الحالة <b class="text-danger">*</b></label>
                                <select class="form-control" id="status" name="status">
                                    <option value="1" {{ ($delivery->status == 1) ? 'selected' : '' }}>قيد التهيئة</option>
                                    <option value="2" {{ ($delivery->status == 2) ? 'selected' : '' }}>مكتمل</option>
                                </select>
                                @error('status')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="col-md-12">
                                <label class="form-label" for="notes">ملاحظات</label>
                                <textarea class="form-control" name="notes" id="notes" cols="30" rows="5">{{ $delivery->notes }}</textarea>
                                @error('notes')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-primary" type="submit">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
