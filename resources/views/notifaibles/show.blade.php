@extends('layouts.app')

@section('content')
    <!-- /.row-->
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-6">
            <div class="card mb-4">
                <div class="card-header"> معلومات الحهة المنبهة #{{ $notifaible->id }}</div>
                <div class="card-body text-right">
                    <table class="table border">
                        <tr>
                            <td>الرقم التسلسلي: </td>
                            <td><b>{{ $notifaible->id }}</b></td>
                        </tr>
                        <tr>
                            <td>المحافظة: </td>
                            <td><b>{{ $notifaible->state->name }}</b></td>
                        </tr>
                        <tr>
                            <td>الاسم: </td>
                            <td><b>{{ $notifaible->name }}</b></td>
                        </tr>
                        <tr>
                            <td>البريد الإلكتروني: </td>
                            <td><b>{{ $notifaible->email }}</b></td>
                        </tr>
                        <tr>
                            <td>ملاحظات: </td>
                            <td><b>{{ $notifaible->notes }}</b></td>
                        </tr>
                    </table>

                    <!--QR Code-->
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
