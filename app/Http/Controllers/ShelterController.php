<?php

namespace App\Http\Controllers;

use App\Exports\SheltersExport;
use App\Http\Requests\ShelterRequest;
use App\Models\Shelter;
use App\Models\State;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ShelterController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('shelters.index', [
                'shelters' => Shelter::with('state')->paginate(15),
            ]);
        }
    }

    public function excel(){
        return Excel::download(new SheltersExport, 'shelters.xlsx');
    }

    public function create(){
        return view('shelters.create', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function store(ShelterRequest $request){
        $data = $request->validated();

        Shelter::create($data);

        return redirect(route('shelters.index'))->with('success', 'تمت إضافة الملجأ بنجاح');
    }

    public function show(Shelter $shelter){
        return view('shelters.show', compact('shelter'));
    }

    public function edit(Shelter $shelter){
        return view('shelters.edit', [
            'shelter' => $shelter,
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function update(ShelterRequest $request, Shelter $shelter){
        $data = $request->validated();

        $shelter->update($data);

        return redirect(route('shelters.show', ['shelter' => $shelter->id]))->with('success', 'تم تعديل بيانات الملجأ بنجاح');
    }

    public function destroy(Shelter $shelter){
        $shelter->delete();
        return redirect(route('shelters.index'))->with('success', 'تم حذف الملجأ بنجاح');
    }
}
