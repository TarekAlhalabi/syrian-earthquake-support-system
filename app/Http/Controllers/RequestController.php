<?php

namespace App\Http\Controllers;

use App\Exports\RequestsExport;
use App\Mail\NotificationEmail;
use App\Models\Notifaible;
use App\Models\Request as ModelsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class RequestController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){            
            return $this->excel();
        }else{
            return view('requests.index', [
                'requests' => \App\Models\Request::with('state')->paginate(15),
            ]);
        }
    }

    public function excel(){
        return Excel::download(new RequestsExport, 'requests.xlsx');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'mobile' => 'required',
            'category' => 'required|numeric|in:1,2,3,4',
            'state_id' => 'required|numeric|exists:states,id',
            'area' => 'required|string',
            'address' => 'required|string',
        ]);

        $request_details = \App\Models\Request::create($request->all());

        //send the email
        $notifaibles = Notifaible::where('state_id', $request->state_id)->get();
        foreach($notifaibles as $n){
            Mail::to($n->email)->send(new NotificationEmail($request_details));
        }

        return redirect()->back()->with('success', 'تم');
    }

    public function edit(\App\Models\Request $request){
        return view('requests.edit', compact('request'));
    }

    public function update($request, Request $request_http){
        $request = \App\Models\Request::withoutGlobalScopes()->find($request);
        $request_http->validate([
            'status' => 'required|numeric',
            'notes' => ''
        ]);

        $request->status = $request_http->status;
        $request->notes = $request_http->notes;
        $request->save();

        return redirect(route('requests.index'))->with('success', 'تم تعديل بيانات الطلب بنجاح');
    }

    public function destroy($request){
        $request = \App\Models\Request::withoutGlobalScopes()->find($request);
        $request->delete();

        return redirect(route('requests.index'))->with('success', 'تم حذف الطلب بنجاح');
    }
}
