<?php

namespace App\Http\Controllers;

use App\Exports\TeamsExport;
use App\Http\Requests\TeamRequest;
use App\Models\State;
use App\Models\Team;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TeamController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('teams.index', [
                'teams' => Team::with('state')->paginate(15),
            ]);
        }
    }

    public function excel(){
        return Excel::download(new TeamsExport, 'teams.xlsx');
    }

    public function create(){
        return view('teams.create', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function store(TeamRequest $request){
        $data = $request->validated();

        Team::create($data);

        return redirect(route('teams.index'))->with('success', 'تمت إضافة الفريق بنجاح');
    }

    public function show(Team $team){
        return view('teams.show', compact('team'));
    }

    public function edit(Team $team){
        return view('teams.edit', [
            'team' => $team,
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function update(TeamRequest $request, Team $team){
        $data = $request->validated();

        $team->update($data);

        return redirect(route('teams.show', ['team' => $team->id]))->with('success', 'تم تعديل بيانات الفريق بنجاح');
    }

    public function destroy(Team $team){
        $team->delete();
        return redirect(route('teams.index'))->with('success', 'تم حذف الفريق بنجاح');
    }
}
