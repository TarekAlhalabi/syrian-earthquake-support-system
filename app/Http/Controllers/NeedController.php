<?php

namespace App\Http\Controllers;

use App\Exports\NeedsExport;
use App\Http\Requests\NeedRequest;
use App\Models\DonationCenter;
use App\Models\Need;
use App\Models\NeedCategory;
use App\Models\NeedName;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class NeedController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('needs.index', [
                'needs' => Need::with(['donation_center', 'donation_center', 'need_name'])->paginate(15),
                'need_categories' => NeedCategory::orderBy('name', 'asc')->get(),
                'donation_centers' => DonationCenter::orderBy('name', 'asc')->get(),
            ]);
        }
    }

    public function excel(){
        return Excel::download(new NeedsExport, 'needs.xlsx');
    }

    public function create(){
        return view('needs.create', [
            'need_categories' => NeedCategory::orderBy('name', 'asc')->get(),
            'donation_centers' => DonationCenter::orderBy('name', 'asc')->get(),
            'need_names' => NeedName::orderBy('name', 'asc')->get(),
        ]);
    }

    public function store(NeedRequest $request){
        $data = $request->validated();

        Need::create($data);

        return redirect(route('needs.index'))->with('success', 'تمت إضافة العنصر بنجاح');
    }

    public function show(Need $need){
        return view('needs.show', compact('need'));
    }

    public function edit(Need $need){
        return view('needs.edit', [
            'need' => $need,
            'need_categories' => NeedCategory::orderBy('name', 'asc')->get(),
            'donation_centers' => DonationCenter::orderBy('name', 'asc')->get(),
            'need_names' => NeedName::orderBy('name', 'asc')->get(),
        ]);
    }

    public function update(NeedRequest $request, Need $need){
        $data = $request->validated();

        $need->update($data);

        return redirect(route('needs.show', ['need' => $need->id]))->with('success', 'تم تعديل العنصر بنجاح');
    }

    public function destroy(Need $need){
        $need->delete();
        return redirect(route('needs.index'))->with('success', 'تم حذف العنصر بنجاح');
    }
}
