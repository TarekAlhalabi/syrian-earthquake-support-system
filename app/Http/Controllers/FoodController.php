<?php

namespace App\Http\Controllers;

use App\Exports\FoodsExport;
use App\Http\Requests\FoodRequest;
use App\Models\Food;
use App\Models\State;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FoodController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('foods.index', [
                'foods' => Food::with('state')->paginate(15),
                'states' => State::orderBy('name', 'asc')->get()
            ]);
        }
    }

    public function excel(){
        return Excel::download(new FoodsExport, 'deliveries.xlsx');
    }

    public function create(){
        return view('foods.create', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function store(FoodRequest $request){
        $data = $request->validated();

        Food::create($data);

        return redirect(route('foods.index'))->with('success', 'تمت إضافة الغذاء بنجاح');
    }

    public function show(Food $food){
        return view('foods.show', compact('food'));
    }

    public function edit(Food $food){
        return view('foods.edit', [
            'food' => $food,
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function update(FoodRequest $request, Food $food){
        $data = $request->validated();

        $food->update($data);

        return redirect(route('foods.show', ['food' => $food->id]))->with('success', 'تم تعديل الغذاء بنجاح');
    }

    public function destroy(Food $food){
        $food->delete();
        return redirect(route('foods.index'))->with('success', 'تم حذف الغذاء بنجاح');
    }
}
