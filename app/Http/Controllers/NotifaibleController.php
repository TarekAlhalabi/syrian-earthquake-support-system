<?php

namespace App\Http\Controllers;

use App\Exports\NotifaiblesExport;
use App\Http\Requests\NotifaibleRequest;
use App\Models\Notifaible;
use App\Models\State;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class NotifaibleController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('notifaibles.index', [
                'notifaibles' => Notifaible::with('state')->paginate(15),
                'states' => State::orderBy('name', 'asc')->get()
            ]);
        }
    }

    public function excel(){
        return Excel::download(new NotifaiblesExport, 'notifaibles.xlsx');
    }

    public function create(){
        return view('notifaibles.create', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function store(NotifaibleRequest $request){
        $data = $request->validated();

        Notifaible::create($data);

        return redirect(route('notifaibles.index'))->with('success', 'تمت إضافة الجهة بنجاح');
    }

    public function show(Notifaible $notifaible){
        return view('notifaibles.show', compact('notifaible'));
    }

    public function edit(Notifaible $notifaible){
        return view('notifaibles.edit', [
            'notifaible' => $notifaible,
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function update(NotifaibleRequest $request, Notifaible $notifaible){
        $data = $request->validated();

        $notifaible->update($data);

        return redirect(route('notifaibles.show', ['notifaible' => $notifaible->id]))->with('success', 'تم تعديل الجهة بنجاح');
    }

    public function destroy(Notifaible $notifaible){
        $notifaible->delete();
        return redirect(route('notifaibles.index'))->with('success', 'تم حذف الجهة بنجاح');
    }
}
