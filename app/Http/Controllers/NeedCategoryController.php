<?php

namespace App\Http\Controllers;

use App\Http\Requests\NeedCategoryRequest;
use App\Models\NeedCategory;
use Illuminate\Http\Request;

class NeedCategoryController extends Controller
{
    public function index(){
        return view('need_categories.index', [
            'need_categories' => NeedCategory::paginate(15),
        ]);
    }

    public function create(){
        return view('need_categories.create');
    }

    public function store(NeedCategoryRequest $request){
        $data = $request->validated();

        NeedCategory::create($data);

        return redirect(route('need_categories.index'))->with('success', 'تمت إضافة فئة الاحتياجات بنجاح');
    }

    public function show(NeedCategory $need_category){
        return view('need_categories.show', compact('need_category'));
    }

    public function edit(NeedCategory $need_category){
        return view('need_categories.edit', [
            'need_category' => $need_category,
        ]);
    }

    public function update(NeedCategoryRequest $request, NeedCategory $need_category){
        $data = $request->validated();

        $need_category->update($data);

        return redirect(route('need_categories.show', ['need_category' => $need_category->id]))->with('success', 'تم تعديل فئة الاحتياجات بنجاح');
    }

    public function destroy(NeedCategory $need_category){
        $need_category->delete();
        return redirect(route('need_categories.index'))->with('success', 'تم حذف فئة الاحتياجات بنجاح');
    }
}
