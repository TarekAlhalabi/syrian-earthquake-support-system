<?php

namespace App\Http\Controllers;

use App\Http\Requests\NeedNameRequest;
use App\Models\NeedName;
use Illuminate\Http\Request;

class NeedNameController extends Controller
{
    public function index(){
        return view('need_names.index', [
            'need_names' => NeedName::paginate(15),
        ]);
    }

    public function create(){
        return view('need_names.create');
    }

    public function store(NeedNameRequest $request){
        $data = $request->validated();

        NeedName::create($data);

        return redirect(route('need_names.index'))->with('success', 'تمت إضافة اسم المادة بنجاح');
    }

    public function show(NeedName $need_name){
        return view('need_names.show', compact('need_name'));
    }

    public function edit(NeedName $need_name){
        return view('need_names.edit', [
            'need_name' => $need_name,
        ]);
    }

    public function update(NeedNameRequest $request, NeedName $need_name){
        $data = $request->validated();

        $need_name->update($data);

        return redirect(route('need_names.show', ['need_name' => $need_name->id]))->with('success', 'تم تعديل اسم المادة بنجاح');
    }

    public function destroy(NeedName $need_name){
        $need_name->delete();
        return redirect(route('need_names.index'))->with('success', 'تم حذف اسم المادة بنجاح');
    }
}
