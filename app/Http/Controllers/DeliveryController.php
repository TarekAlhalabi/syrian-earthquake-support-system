<?php

namespace App\Http\Controllers;

use App\Exports\DeliveriesExport;
use App\Http\Requests\DeliveryRequest;
use App\Models\Delivery;
use App\Models\State;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DeliveryController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('deliveries.index', [
                'deliveries' => Delivery::paginate(15),
            ]);
        }
    }

    public function excel(){
        return Excel::download(new DeliveriesExport, 'deliveries.xlsx');
    }

    public function create(){
        return view('deliveries.create');
    }

    public function store(DeliveryRequest $request){
        $data = $request->validated();

        Delivery::create($data);

        return redirect(route('deliveries.index'))->with('success', 'تمت إضافة طلب النقل بنجاح');
    }

    public function show(Delivery $delivery){
        return view('deliveries.show', compact('delivery'));
    }

    public function edit(Delivery $delivery){
        return view('deliveries.edit', [
            'delivery' => $delivery,
        ]);
    }

    public function update(DeliveryRequest $request, Delivery $delivery){
        $data = $request->validated();

        $delivery->update($data);

        return redirect(route('deliveries.show', ['delivery' => $delivery->id]))->with('success', 'تم تعديل بيانات طلب النقل بنجاح');
    }

    public function destroy(Delivery $delivery){
        $delivery->delete();
        return redirect(route('deliveries.index'))->with('success', 'تم حذف طلب النقل بنجاح');
    }
}
