<?php

namespace App\Http\Controllers;

use App\Exports\MissingsExport;
use App\Http\Requests\MissingRequest;
use App\Models\Missing;
use App\Models\State;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MissingController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('missings.index', [
                'missings' => Missing::with('state')->paginate(15),
                'states' => State::orderBy('name', 'asc')->get()
            ]);
        }
    }

    public function excel(){
        return Excel::download(new MissingsExport, 'missings.xlsx');
    }

    public function create(){
        return view('missings.create', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function store(MissingRequest $request){
        $data = $request->validated();

        $request->validate([
            'image' => 'required'
        ]);

        if($request->hasFile('image'))
            $data['image'] = $request->image->store('public');

        Missing::create($data);

        return redirect(route('missings.index'))->with('success', 'تمت إضافة الحالة بنجاح');
    }

    public function show(Missing $missing){
        return view('missings.show', compact('missing'));
    }

    public function edit(Missing $missing){
        return view('missings.edit', [
            'missing' => $missing,
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function update(MissingRequest $request, Missing $missing){
        $data = $request->validated();

        if($request->hasFile('image'))
            $data['image'] = $request->image->store('public');

        $missing->update($data);

        return redirect(route('missings.show', ['missing' => $missing->id]))->with('success', 'تم تعديل بيانات الحالة بنجاح');
    }

    public function destroy(Missing $missing){
        $missing->delete();
        return redirect(route('missings.index'))->with('success', 'تم حذف الحالة بنجاح');
    }
}
