<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use App\Models\Shelter;
use App\Models\State;
use App\Models\Team;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
        return view('front.index', [
            'teams' => Team::with('state')->limit(4)->get(),
            'donations' => Donation::limit(6)->get(),
            'shelters' => Shelter::with('state')->limit(6)->get()
        ]);
    }

    public function food_support(){
        return view('front.food_support_request', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function medical_support(){
        return view('front.medical_support_request', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function shelter_support(){
        return view('front.shelter_support_request', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function stuck_in_rubble(){
        return view('front.stuck_in_rubble_request', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function teams(){
        return view('front.teams', [
            'teams' => Team::with('state')->paginate(15),
        ]);
    }

    public function donations(){
        return view('front.donations', [
            'donations' => Donation::paginate(15),
        ]);
    }

    public function shelters(){
        return view('front.shelters', [
            'shelters' => Shelter::with('state')->paginate(15),
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }
}
