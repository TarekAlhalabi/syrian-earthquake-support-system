<?php

namespace App\Http\Controllers;

use App\Exports\SupportsExport;
use App\Http\Requests\SupportRequest;
use App\Models\State;
use App\Models\Support;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SupportController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('supports.index', [
                'supports' => Support::with('state')->paginate(15),
                'states' => State::orderBy('name', 'asc')->get()
            ]);
        }
    }

    public function excel(){
        return Excel::download(new SupportsExport, 'supports.xlsx');
    }

    public function create(){
        return view('supports.create', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function store(SupportRequest $request){
        $data = $request->validated();

        Support::create($data);

        return redirect(route('supports.index'))->with('success', 'تمت إضافة طلب المتطوعين بنجاح');
    }

    public function show(Support $support){
        return view('supports.show', compact('support'));
    }

    public function edit(Support $support){
        return view('supports.edit', [
            'support' => $support,
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function update(SupportRequest $request, Support $support){
        $data = $request->validated();

        $support->update($data);

        return redirect(route('supports.show', ['support' => $support->id]))->with('success', 'تم تعديل بيانات طلب المتطوعين بنجاح');
    }

    public function destroy(Support $support){
        $support->delete();
        return redirect(route('supports.index'))->with('success', 'تم حذف طلب المتطوعين بنجاح');
    }
}
