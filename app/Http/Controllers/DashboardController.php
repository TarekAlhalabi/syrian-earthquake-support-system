<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\Missing;
use App\Models\Shelter;
use App\Models\Support;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', [
            'missings' => Missing::with('state')->paginate(15),
            'missings_count' => Missing::where('status', 1)->count(),
            'shelters_count' => Shelter::where('status', 1)->count(),
            'deliveries_count' => Delivery::where('status', 1)->count(),
            'supports_count' => Support::where('status', 1)->count(),
        ]);
    }
}
