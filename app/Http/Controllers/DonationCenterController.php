<?php

namespace App\Http\Controllers;

use App\Exports\DonationCentersExport;
use App\Http\Requests\DonationCenterRequest;
use App\Models\DonationCenter;
use App\Models\State;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DonationCenterController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('donation_centers.index', [
                'donation_centers' => DonationCenter::with('state')->paginate(15),
                'states' => State::orderBy('name', 'asc')->get()
            ]);
        }
    }

    public function excel(){
        return Excel::download(new DonationCentersExport, 'donation_centers.xlsx');
    }

    public function create(){
        return view('donation_centers.create', [
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function store(DonationCenterRequest $request){
        $data = $request->validated();

        DonationCenter::create($data);

        return redirect(route('donation_centers.index'))->with('success', 'تمت إضافة مركز التبرع بنجاح');
    }

    public function show(DonationCenter $donation_center){
        return view('donation_centers.show', compact('donation_center'));
    }

    public function edit(DonationCenter $donation_center){
        return view('donation_centers.edit', [
            'donation_center' => $donation_center,
            'states' => State::orderBy('name', 'asc')->get()
        ]);
    }

    public function update(DonationCenterRequest $request, DonationCenter $donation_center){
        $data = $request->validated();

        $donation_center->update($data);

        return redirect(route('donation_centers.show', ['donation_center' => $donation_center->id]))->with('success', 'تم تعديل بيانات مركز التبرع بنجاح');
    }

    public function destroy(DonationCenter $donation_center){
        $donation_center->delete();
        return redirect(route('donation_centers.index'))->with('success', 'تم حذف مركز التبرع بنجاح');
    }
}
