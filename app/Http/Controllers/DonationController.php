<?php

namespace App\Http\Controllers;

use App\Exports\DonationsExport;
use App\Http\Requests\DonationRequest;
use App\Models\Donation;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DonationController extends Controller
{
    public function index(Request $request){
        if($request->submit == 'export_excel'){
            return $this->excel();
        }else{
            return view('donations.index', [
                'donations' => Donation::paginate(15),
            ]);
        }
    }

    public function excel(){
        return Excel::download(new DonationsExport, 'donations.xlsx');
    }

    public function create(){
        return view('donations.create');
    }

    public function store(DonationRequest $request){
        $data = $request->validated();

        $request->validate([
            'image' => 'required'
        ]);

        if($request->hasFile('image'))
            $data['image'] = $request->image->store('public');

        Donation::create($data);

        return redirect(route('donations.index'))->with('success', 'تمت إضافة الحملة التطوعية بنجاح');
    }

    public function show(Donation $donation){
        return view('donations.show', compact('donation'));
    }

    public function edit(Donation $donation){
        return view('donations.edit', [
            'donation' => $donation,
        ]);
    }

    public function update(DonationRequest $request, Donation $donation){
        $data = $request->validated();

        if($request->hasFile('image'))
            $data['image'] = $request->image->store('public');

        $donation->update($data);

        return redirect(route('donations.show', ['donation' => $donation->id]))->with('success', 'تم تعديل بيانات الحملة التطوعية بنجاح');
    }

    public function destroy(Donation $donation){
        $donation->delete();
        return redirect(route('donations.index'))->with('success', 'تم حذف الحملة التطوعية بنجاح');
    }
}
