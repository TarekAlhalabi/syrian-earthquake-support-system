<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NeedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'need_category_id' => 'required|numeric|exists:need_categories,id',
            'donation_center_id' => 'required|numeric|exists:donation_centers,id',
            'need_name_id' => 'required|numeric|exists:need_names,id',
            'notes' => '',
            'cost' => 'required|numeric',
            'quantity' => 'required|numeric',
            'type' => 'required|numeric',
        ];
    }
}
