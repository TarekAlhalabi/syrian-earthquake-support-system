<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'address' => 'required|string',
            'responsible' => 'required|string',
            'quantity' => 'required|numeric',
            'status' => 'required|numeric',
            'state_id' => 'required|numeric|exists:states,id',
            'notes' => ''
        ];
    }
}
