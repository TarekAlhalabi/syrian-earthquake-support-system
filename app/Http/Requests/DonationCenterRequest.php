<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DonationCenterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'area' => 'required|string',
            'address' => 'required|string',
            'responsible' => 'required|string',
            'contact_number' => 'required|string',
            'notes' => '',
            'state_id' => 'required|numeric|exists:states,id',
        ];
    }
}
