<?php

namespace App\Exports;

use App\Models\Shelter;
use Maatwebsite\Excel\Concerns\FromCollection;

class SheltersExport implements FromCollection
{
    public function collection()
    {
        return Shelter::all();
    }
}