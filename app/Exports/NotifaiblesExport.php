<?php

namespace App\Exports;

use App\Models\Notifaible;
use Maatwebsite\Excel\Concerns\FromCollection;

class NotifaiblesExport implements FromCollection
{
    public function collection()
    {
        return Notifaible::all();
    }
}