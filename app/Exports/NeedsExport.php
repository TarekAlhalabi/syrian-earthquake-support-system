<?php

namespace App\Exports;

use App\Models\Need;
use Maatwebsite\Excel\Concerns\FromCollection;

class NeedsExport implements FromCollection
{
    public function collection()
    {
        return Need::all();
    }
}