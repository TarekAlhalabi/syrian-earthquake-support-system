<?php

namespace App\Exports;

use App\Models\Team;
use Maatwebsite\Excel\Concerns\FromCollection;

class TeamsExport implements FromCollection
{
    public function collection()
    {
        return Team::all();
    }
}