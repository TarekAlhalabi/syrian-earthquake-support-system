<?php

namespace App\Exports;

use App\Models\DonationCenter;
use Maatwebsite\Excel\Concerns\FromCollection;

class DonationCentersExport implements FromCollection
{
    public function collection()
    {
        return DonationCenter::all();
    }
}