<?php

namespace App\Exports;

use App\Models\Missing;
use Maatwebsite\Excel\Concerns\FromCollection;

class MissingsExport implements FromCollection
{
    public function collection()
    {
        return Missing::all();
    }
}