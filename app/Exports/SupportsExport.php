<?php

namespace App\Exports;

use App\Models\Support;
use Maatwebsite\Excel\Concerns\FromCollection;

class SupportsExport implements FromCollection
{
    public function collection()
    {
        return Support::all();
    }
}