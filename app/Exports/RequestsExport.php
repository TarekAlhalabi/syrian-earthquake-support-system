<?php

namespace App\Exports;

use App\Models\Request;
use Maatwebsite\Excel\Concerns\FromCollection;

class RequestsExport implements FromCollection
{
    public function collection()
    {
        return Request::all();
    }
}