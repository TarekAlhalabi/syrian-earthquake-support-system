<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shelter extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'responsible',
        'status',
        'capacity',
        'notes',
        'state_id',
        'added_by'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
        static::creating(function (Shelter $model) {
            $model->added_by = auth('sanctum')->id();
        });
    }

    public function get_status(){
        if($this->status == 1) return 'متاح';
        else return 'ممتلئ';
    }

    public function state(){
        return $this->belongsTo(State::class);
    }
}
