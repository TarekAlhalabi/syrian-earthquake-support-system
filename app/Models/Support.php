<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'state_id',
        'added_by',
        'notes',
        'responsible',
        'status'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
        static::creating(function (Support $model) {
            $model->added_by = auth('sanctum')->id();
        });
    }

    public function state(){
        return $this->belongsTo(State::class);
    }
}
