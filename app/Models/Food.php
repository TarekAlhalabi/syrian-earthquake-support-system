<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'responsible',
        'status',
        'quantity',
        'notes',
        'state_id',
        'added_by'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
        static::creating(function (Food $model) {
            $model->added_by = auth('sanctum')->id();
        });
    }

    public function state(){
        return $this->belongsTo(State::class);
    }
}
