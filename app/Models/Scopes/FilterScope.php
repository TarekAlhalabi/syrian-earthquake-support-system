<?php
 
namespace App\Models\Scopes;
 
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
 
class FilterScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if(request()->has('from') && request()->get('from'))
            $builder->whereDate('created_at', '>=', request()->get('from'));

        if(request()->has('to') && request()->get('to'))
            $builder->whereDate('created_at', '<=', request()->get('to'));

        if(request()->has('name'))
            $builder->where('name', 'LIKE' , "%" . request()->get('name'). "%");

        if(request()->has('status') && request()->get('status') != 0)
            $builder->where('status', request()->get('status'));

        if(request()->has('state') && request()->get('state') != 0)
            $builder->where('state_id', request()->get('state'));

        if(request()->has('sort') && request()->get('sort') == 0)
            $builder->orderBy('created_at', 'asc');
        else
            $builder->orderBy('created_at', 'desc');
    }
}