<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'image',
        'notes',
        'mobile',
        'added_by'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
        static::creating(function (Donation $model) {
            $model->added_by = auth('sanctum')->id();
        });
    }

    public function getImageAttribute(){
        return asset('storage/' . explode('/', $this->attributes['image'])[1]);
    }
}
