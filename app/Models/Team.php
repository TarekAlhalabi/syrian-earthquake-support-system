<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'primary_mobile',
        'secondary_mobile',
        'state_id',
        'added_by',
        'notes'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
        static::creating(function (Team $model) {
            $model->added_by = auth('sanctum')->id();
        });
    }

    public function state(){
        return $this->belongsTo(State::class);
    }
}
