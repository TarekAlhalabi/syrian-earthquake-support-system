<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'mobile',
        'notes',
        'status',
        'category',
        'state_id',
        'area',
        'address',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
    }

    public function getCategoryAttribute(){
        if($this->attributes['category'] == 1){
            return 'مساعدة صحية';
        }elseif($this->attributes['category'] == 2){
            return 'مساعدة غذائية';
        }elseif($this->attributes['category'] == 3){
            return 'مساعدة مأوى';
        }else{
            return 'مساعدة شخص عالق';
        }
    }

    public function state(){
        return $this->belongsTo(State::class);
    }
}
