<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Need extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'need_category_id',
        'need_name_id',
        'donation_center_id',
        'added_by',
        'cost',
        'notes',
        'quantity',
        'type',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
        static::creating(function (Need $model) {
            $model->added_by = auth('sanctum')->id();
        });
    }

    public function need_category(){
        return $this->belongsTo(NeedCategory::class);
    }

    public function donation_center(){
        return $this->belongsTo(DonationCenter::class);
    }

    public function need_name(){
        return $this->belongsTo(NeedName::class);
    }
}
