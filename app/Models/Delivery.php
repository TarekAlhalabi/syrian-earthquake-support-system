<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'from',
        'to',
        'responsible',
        'status',
        'notes',
        'added_by'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
        static::creating(function (Delivery $model) {
            $model->added_by = auth('sanctum')->id();
        });
    }
}
