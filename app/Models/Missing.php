<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Missing extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'age',
        'image',
        'health_condition',
        'state_id',
        'address',
        'notes',
        'responsible',
        'status',
        'added_by',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
        static::creating(function (Missing $model) {
            $model->added_by = auth('sanctum')->id();
        });
    }

    public function getImageAttribute(){
        return asset('storage/' . explode('/', $this->attributes['image'])[1]);
    }

    public function getHealthConditionAttribute(){
        if($this->attributes['health_condition'] == 1) return 'سليم';
        else return 'جريح';
    }

    public function state(){
        return $this->belongsTo(State::class);
    }
}
