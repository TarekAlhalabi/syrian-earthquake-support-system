<?php

namespace App\Models;

use App\Models\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonationCenter extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'area',
        'address',
        'responsible',
        'contact_number',
        'notes',
        'state_id',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope);
    }

    public function state(){
        return $this->belongsTo(State::class);
    }
}
