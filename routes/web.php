<?php

use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\DonationCenterController;
use App\Http\Controllers\DonationController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\MissingController;
use App\Http\Controllers\NeedCategoryController;
use App\Http\Controllers\NeedController;
use App\Http\Controllers\NeedNameController;
use App\Http\Controllers\NotifaibleController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\ShelterController;
use App\Http\Controllers\SupportController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, 'index']);
Route::get('/food-support', [FrontController::class, 'food_support']);
Route::get('/stuck-in-rubble-report', [FrontController::class, 'stuck_in_rubble']);
Route::get('/shelter-support', [FrontController::class, 'shelter_support']);
Route::get('/medical-support', [FrontController::class, 'medical_support']);
Route::get('/f/teams', [FrontController::class, 'teams']);
Route::get('/f/donations', [FrontController::class, 'donations']);
Route::get('/f/shelters', [FrontController::class, 'shelters']);
Route::post('/requests', [RequestController::class, 'store'])->name('requests.store');

Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

Route::group(['middleware' => 'auth'], function(){
    Route::resource('/missings', MissingController::class, ['names' => 'missings']);
    Route::resource('/shelters', ShelterController::class, ['names' => 'shelters']);
    Route::resource('/foods', FoodController::class, ['names' => 'foods']);
    Route::resource('/deliveries', DeliveryController::class, ['names' => 'deliveries']);
    Route::resource('/supports', SupportController::class, ['names' => 'supports']);
    Route::resource('/requests', RequestController::class, ['names' => 'requests'])->except('store');
    Route::resource('/teams', TeamController::class, ['names' => 'teams']);
    Route::resource('/donations', DonationController::class, ['names' => 'donations']);
    Route::resource('/notifaibles', NotifaibleController::class, ['names' => 'notifaibles']);
    Route::resource('/need_categories', NeedCategoryController::class, ['names' => 'need_categories']);
    Route::resource('/donation_centers', DonationCenterController::class, ['names' => 'donation_centers']);
    Route::resource('/needs', NeedController::class, ['names' => 'needs']);
    Route::resource('/need_names', NeedNameController::class, ['names' => 'need_names']);
});
